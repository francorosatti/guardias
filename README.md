# Guardias

Aplicación para armar las guardias medicas de un mes para un grupo de trabajo.

### Grupos de Residentes

Permite crear grupos de residentes y diagramar sus guardias

### Informe

Imprime en pdf el calendario de guardias

### A Futuro

Tener una estadistica de las guardias realizadas.