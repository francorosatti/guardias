﻿'**********************************************************************
'* Name: FormExcepcion.vb
'* Desc: Fromulario para administrar una excepcion
'* Auth: Franco Rosatti
'* Date: Sep-2019
'* Vers: 1.0
'**********************************************************************

Public Class FormExcepcion
#Region "INICIO Y FIN"
    Public Sub New(mes As Integer, año As Integer, residentes As Integer, Optional value As Excepcion = Nothing)
        ' This call is required by the designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.

        If value Is Nothing Then
            Me.Excepcion = New Excepcion
            Me.Text = "Nueva Excepción"
        Else
            Me.Excepcion = value
            Me.Text = "Modificar Excepción"
        End If

        Me.NUD_Residentes.Maximum = residentes

        Me.DTP_Fecha.MinDate = New Date(año, mes, 1)
        Me.DTP_Fecha.MaxDate = New Date(año, mes, DateTime.DaysInMonth(año, mes))
    End Sub
    Private Sub FormExcepcion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Cargar()
    End Sub
#End Region
#Region "VARIABLES Y PROPIEDADES"
    Public ReadOnly Property Excepcion As Excepcion
#End Region
#Region "ACCIONES DE LOS USUARIOS"
    Private Sub B_Aceptar_Click(sender As Object, e As EventArgs) Handles B_Aceptar.Click
        Me.Guardar()
    End Sub
    Private Sub CB_Excepcion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CB_Excepcion.SelectedIndexChanged
        If Me.CB_Excepcion.SelectedIndex = 0 Then
            Me.L_Residentes.Visible = True
            Me.NUD_Residentes.Visible = True
        Else
            Me.L_Residentes.Visible = False
            Me.NUD_Residentes.Visible = False
        End If
    End Sub
#End Region
#Region "METODOS PRIVADOS"
    Private Sub Cargar()
        If Me.Excepcion.Laboral Then
            Me.CB_Excepcion.SelectedIndex = 0
        Else
            Me.CB_Excepcion.SelectedIndex = 1
        End If

        If Me.Excepcion.Fecha >= Me.DTP_Fecha.MinDate AndAlso Me.Excepcion.Fecha <= Me.DTP_Fecha.MaxDate Then
            Me.DTP_Fecha.Value = Me.Excepcion.Fecha
        Else
            Me.DTP_Fecha.Value = Me.DTP_Fecha.MinDate
        End If

        Me.NUD_Residentes.Value = Math.Max(Me.NUD_Residentes.Minimum, Math.Min(Me.NUD_Residentes.Maximum, Me.Excepcion.ResidentesDeGuardia))
    End Sub
    Private Sub Guardar()
        If Me.Validar() Then
            Me.Excepcion.Fecha = Me.DTP_Fecha.Value
            Me.Excepcion.Laboral = IIf(Me.CB_Excepcion.SelectedIndex = 0, True, False)
            If Me.Excepcion.Laboral Then
                Me.Excepcion.ResidentesDeGuardia = Me.NUD_Residentes.Value
            Else
                Me.Excepcion.ResidentesDeGuardia = 0
            End If
            Me.DialogResult = DialogResult.OK
        End If
    End Sub
    Private Function Validar() As Boolean
        Dim msg As String = ""

        If Me.DTP_Fecha.Value = Nothing Then
            msg += "• Seleccione una fecha." + vbCrLf
        End If

        If Me.CB_Excepcion.SelectedIndex < 0 OrElse Me.CB_Excepcion.SelectedIndex >= Me.CB_Excepcion.Items.Count Then
            msg += "• Seleccione el tipo de excepción." + vbCrLf
        Else
            If Me.CB_Excepcion.SelectedIndex = 0 Then
                If Me.NUD_Residentes.Value <= 0 Then
                    msg += "• Debe haber al menos un residente de guardia."
                End If
            End If
        End If

            If msg <> "" Then
                MsgBox(msg, Buttons:=MsgBoxStyle.Exclamation Or MsgBoxStyle.OkOnly, Title:=TextoService.AppFullName)
                Return False
            End If
            Return True
    End Function
#End Region
End Class