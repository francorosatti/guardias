﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormResidente
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormResidente))
        Me.TB_Nombre = New System.Windows.Forms.TextBox()
        Me.TB_Apellido = New System.Windows.Forms.TextBox()
        Me.L_Nombre = New System.Windows.Forms.Label()
        Me.L_Apellido = New System.Windows.Forms.Label()
        Me.B_Cancelar = New System.Windows.Forms.Button()
        Me.B_Aceptar = New System.Windows.Forms.Button()
        Me.P_Gris = New System.Windows.Forms.Panel()
        Me.P_Blanco = New System.Windows.Forms.Panel()
        Me.SuspendLayout()
        '
        'TB_Nombre
        '
        Me.TB_Nombre.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TB_Nombre.Location = New System.Drawing.Point(98, 12)
        Me.TB_Nombre.MaxLength = 50
        Me.TB_Nombre.Name = "TB_Nombre"
        Me.TB_Nombre.Size = New System.Drawing.Size(292, 22)
        Me.TB_Nombre.TabIndex = 1
        '
        'TB_Apellido
        '
        Me.TB_Apellido.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TB_Apellido.Location = New System.Drawing.Point(98, 40)
        Me.TB_Apellido.MaxLength = 50
        Me.TB_Apellido.Name = "TB_Apellido"
        Me.TB_Apellido.Size = New System.Drawing.Size(292, 22)
        Me.TB_Apellido.TabIndex = 3
        '
        'L_Nombre
        '
        Me.L_Nombre.Location = New System.Drawing.Point(12, 15)
        Me.L_Nombre.Name = "L_Nombre"
        Me.L_Nombre.Size = New System.Drawing.Size(80, 24)
        Me.L_Nombre.TabIndex = 0
        Me.L_Nombre.Text = "Nombre:"
        '
        'L_Apellido
        '
        Me.L_Apellido.Location = New System.Drawing.Point(12, 43)
        Me.L_Apellido.Name = "L_Apellido"
        Me.L_Apellido.Size = New System.Drawing.Size(80, 24)
        Me.L_Apellido.TabIndex = 2
        Me.L_Apellido.Text = "Apellido:"
        '
        'B_Cancelar
        '
        Me.B_Cancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.B_Cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.B_Cancelar.Location = New System.Drawing.Point(300, 89)
        Me.B_Cancelar.Name = "B_Cancelar"
        Me.B_Cancelar.Size = New System.Drawing.Size(90, 32)
        Me.B_Cancelar.TabIndex = 7
        Me.B_Cancelar.Text = "Cancelar"
        Me.B_Cancelar.UseVisualStyleBackColor = True
        '
        'B_Aceptar
        '
        Me.B_Aceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.B_Aceptar.Location = New System.Drawing.Point(204, 89)
        Me.B_Aceptar.Name = "B_Aceptar"
        Me.B_Aceptar.Size = New System.Drawing.Size(90, 32)
        Me.B_Aceptar.TabIndex = 6
        Me.B_Aceptar.Text = "Aceptar"
        Me.B_Aceptar.UseVisualStyleBackColor = True
        '
        'P_Gris
        '
        Me.P_Gris.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.P_Gris.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.P_Gris.Location = New System.Drawing.Point(-2, 76)
        Me.P_Gris.Name = "P_Gris"
        Me.P_Gris.Size = New System.Drawing.Size(406, 1)
        Me.P_Gris.TabIndex = 5
        '
        'P_Blanco
        '
        Me.P_Blanco.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.P_Blanco.BackColor = System.Drawing.SystemColors.Window
        Me.P_Blanco.Location = New System.Drawing.Point(-2, 77)
        Me.P_Blanco.Name = "P_Blanco"
        Me.P_Blanco.Size = New System.Drawing.Size(406, 1)
        Me.P_Blanco.TabIndex = 4
        '
        'FormResidente
        '
        Me.AcceptButton = Me.B_Aceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(120.0!, 120.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.CancelButton = Me.B_Cancelar
        Me.ClientSize = New System.Drawing.Size(402, 133)
        Me.Controls.Add(Me.P_Blanco)
        Me.Controls.Add(Me.P_Gris)
        Me.Controls.Add(Me.B_Aceptar)
        Me.Controls.Add(Me.B_Cancelar)
        Me.Controls.Add(Me.L_Apellido)
        Me.Controls.Add(Me.L_Nombre)
        Me.Controls.Add(Me.TB_Apellido)
        Me.Controls.Add(Me.TB_Nombre)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormResidente"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Residente"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents TB_Nombre As TextBox
    Private WithEvents TB_Apellido As TextBox
    Private WithEvents L_Nombre As Label
    Private WithEvents L_Apellido As Label
    Private WithEvents B_Cancelar As Button
    Private WithEvents B_Aceptar As Button
    Private WithEvents P_Gris As Panel
    Private WithEvents P_Blanco As Panel
End Class
