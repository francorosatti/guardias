﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormAbout
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormAbout))
        Me.PB_Imagen = New System.Windows.Forms.PictureBox()
        Me.L_Leyenda = New System.Windows.Forms.Label()
        CType(Me.PB_Imagen, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PB_Imagen
        '
        Me.PB_Imagen.BackColor = System.Drawing.SystemColors.Window
        Me.PB_Imagen.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PB_Imagen.Image = CType(resources.GetObject("PB_Imagen.Image"), System.Drawing.Image)
        Me.PB_Imagen.Location = New System.Drawing.Point(0, 0)
        Me.PB_Imagen.Name = "PB_Imagen"
        Me.PB_Imagen.Size = New System.Drawing.Size(772, 460)
        Me.PB_Imagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PB_Imagen.TabIndex = 0
        Me.PB_Imagen.TabStop = False
        '
        'L_Leyenda
        '
        Me.L_Leyenda.BackColor = System.Drawing.SystemColors.Control
        Me.L_Leyenda.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.L_Leyenda.Font = New System.Drawing.Font("Consolas", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.L_Leyenda.Location = New System.Drawing.Point(0, 460)
        Me.L_Leyenda.Name = "L_Leyenda"
        Me.L_Leyenda.Size = New System.Drawing.Size(772, 58)
        Me.L_Leyenda.TabIndex = 1
        Me.L_Leyenda.Text = "Guardias 1.0 - 2019 - Franco Rosatti - francorosatti@gmail.com"
        Me.L_Leyenda.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FormAbout
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(120.0!, 120.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ClientSize = New System.Drawing.Size(772, 518)
        Me.Controls.Add(Me.PB_Imagen)
        Me.Controls.Add(Me.L_Leyenda)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormAbout"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Acerca de Guardias"
        Me.TransparencyKey = System.Drawing.SystemColors.Window
        CType(Me.PB_Imagen, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents PB_Imagen As PictureBox
    Private WithEvents L_Leyenda As Label
End Class
