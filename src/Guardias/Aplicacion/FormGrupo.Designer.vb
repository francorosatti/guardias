﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormGrupo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormGrupo))
        Me.L_Nombre = New System.Windows.Forms.Label()
        Me.TB_Nombre = New System.Windows.Forms.TextBox()
        Me.LV_Residentes = New System.Windows.Forms.ListView()
        Me.B_Guardar = New System.Windows.Forms.Button()
        Me.B_Cancelar = New System.Windows.Forms.Button()
        Me.P_Residentes = New System.Windows.Forms.Panel()
        Me.TS_Residentes = New System.Windows.Forms.ToolStrip()
        Me.TSB_CrearResidente = New System.Windows.Forms.ToolStripButton()
        Me.TSB_ModificarResidente = New System.Windows.Forms.ToolStripButton()
        Me.TSB_EliminarResidente = New System.Windows.Forms.ToolStripButton()
        Me.CH_Residente = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.P_Residentes.SuspendLayout()
        Me.TS_Residentes.SuspendLayout()
        Me.SuspendLayout()
        '
        'L_Nombre
        '
        Me.L_Nombre.Location = New System.Drawing.Point(12, 15)
        Me.L_Nombre.Name = "L_Nombre"
        Me.L_Nombre.Size = New System.Drawing.Size(80, 24)
        Me.L_Nombre.TabIndex = 0
        Me.L_Nombre.Text = "Nombre:"
        '
        'TB_Nombre
        '
        Me.TB_Nombre.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TB_Nombre.Location = New System.Drawing.Point(98, 12)
        Me.TB_Nombre.Name = "TB_Nombre"
        Me.TB_Nombre.Size = New System.Drawing.Size(472, 22)
        Me.TB_Nombre.TabIndex = 1
        '
        'LV_Residentes
        '
        Me.LV_Residentes.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.LV_Residentes.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.CH_Residente})
        Me.LV_Residentes.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LV_Residentes.FullRowSelect = True
        Me.LV_Residentes.GridLines = True
        Me.LV_Residentes.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.LV_Residentes.Location = New System.Drawing.Point(0, 40)
        Me.LV_Residentes.MultiSelect = False
        Me.LV_Residentes.Name = "LV_Residentes"
        Me.LV_Residentes.Size = New System.Drawing.Size(556, 207)
        Me.LV_Residentes.TabIndex = 2
        Me.LV_Residentes.UseCompatibleStateImageBehavior = False
        Me.LV_Residentes.View = System.Windows.Forms.View.Details
        '
        'B_Guardar
        '
        Me.B_Guardar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.B_Guardar.Location = New System.Drawing.Point(385, 309)
        Me.B_Guardar.Name = "B_Guardar"
        Me.B_Guardar.Size = New System.Drawing.Size(90, 32)
        Me.B_Guardar.TabIndex = 8
        Me.B_Guardar.Text = "Guardar"
        Me.B_Guardar.UseVisualStyleBackColor = True
        '
        'B_Cancelar
        '
        Me.B_Cancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.B_Cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.B_Cancelar.Location = New System.Drawing.Point(481, 309)
        Me.B_Cancelar.Name = "B_Cancelar"
        Me.B_Cancelar.Size = New System.Drawing.Size(90, 32)
        Me.B_Cancelar.TabIndex = 9
        Me.B_Cancelar.Text = "Cancelar"
        Me.B_Cancelar.UseVisualStyleBackColor = True
        '
        'P_Residentes
        '
        Me.P_Residentes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.P_Residentes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.P_Residentes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.P_Residentes.Controls.Add(Me.LV_Residentes)
        Me.P_Residentes.Controls.Add(Me.TS_Residentes)
        Me.P_Residentes.Location = New System.Drawing.Point(12, 47)
        Me.P_Residentes.Name = "P_Residentes"
        Me.P_Residentes.Size = New System.Drawing.Size(558, 249)
        Me.P_Residentes.TabIndex = 10
        '
        'TS_Residentes
        '
        Me.TS_Residentes.AutoSize = False
        Me.TS_Residentes.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.TS_Residentes.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.TS_Residentes.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSB_CrearResidente, Me.TSB_ModificarResidente, Me.TSB_EliminarResidente})
        Me.TS_Residentes.Location = New System.Drawing.Point(0, 0)
        Me.TS_Residentes.Name = "TS_Residentes"
        Me.TS_Residentes.Size = New System.Drawing.Size(556, 40)
        Me.TS_Residentes.TabIndex = 3
        '
        'TSB_CrearResidente
        '
        Me.TSB_CrearResidente.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.TSB_CrearResidente.Image = CType(resources.GetObject("TSB_CrearResidente.Image"), System.Drawing.Image)
        Me.TSB_CrearResidente.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_CrearResidente.Name = "TSB_CrearResidente"
        Me.TSB_CrearResidente.Size = New System.Drawing.Size(48, 37)
        Me.TSB_CrearResidente.Text = "Crear"
        '
        'TSB_ModificarResidente
        '
        Me.TSB_ModificarResidente.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.TSB_ModificarResidente.Enabled = False
        Me.TSB_ModificarResidente.Image = CType(resources.GetObject("TSB_ModificarResidente.Image"), System.Drawing.Image)
        Me.TSB_ModificarResidente.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_ModificarResidente.Name = "TSB_ModificarResidente"
        Me.TSB_ModificarResidente.Size = New System.Drawing.Size(77, 37)
        Me.TSB_ModificarResidente.Text = "Modificar"
        '
        'TSB_EliminarResidente
        '
        Me.TSB_EliminarResidente.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.TSB_EliminarResidente.Enabled = False
        Me.TSB_EliminarResidente.Image = CType(resources.GetObject("TSB_EliminarResidente.Image"), System.Drawing.Image)
        Me.TSB_EliminarResidente.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_EliminarResidente.Name = "TSB_EliminarResidente"
        Me.TSB_EliminarResidente.Size = New System.Drawing.Size(67, 37)
        Me.TSB_EliminarResidente.Text = "Eliminar"
        '
        'CH_Residente
        '
        Me.CH_Residente.Text = "Residente"
        Me.CH_Residente.Width = 500
        '
        'FormGrupo
        '
        Me.AcceptButton = Me.B_Guardar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(120.0!, 120.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.CancelButton = Me.B_Cancelar
        Me.ClientSize = New System.Drawing.Size(582, 353)
        Me.Controls.Add(Me.P_Residentes)
        Me.Controls.Add(Me.B_Guardar)
        Me.Controls.Add(Me.B_Cancelar)
        Me.Controls.Add(Me.TB_Nombre)
        Me.Controls.Add(Me.L_Nombre)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(600, 400)
        Me.Name = "FormGrupo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Grupo de Residentes"
        Me.P_Residentes.ResumeLayout(False)
        Me.TS_Residentes.ResumeLayout(False)
        Me.TS_Residentes.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents L_Nombre As Label
    Private WithEvents TB_Nombre As TextBox
    Private WithEvents B_Guardar As Button
    Private WithEvents B_Cancelar As Button
    Private WithEvents LV_Residentes As ListView
    Private WithEvents TS_Residentes As ToolStrip
    Private WithEvents TSB_CrearResidente As ToolStripButton
    Private WithEvents TSB_ModificarResidente As ToolStripButton
    Private WithEvents TSB_EliminarResidente As ToolStripButton
    Private WithEvents P_Residentes As Panel
    Private WithEvents CH_Residente As ColumnHeader
End Class
