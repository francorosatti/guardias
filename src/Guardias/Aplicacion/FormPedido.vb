﻿'**********************************************************************
'* Name: FormPedido.vb
'* Desc: Fromulario para administrar un pedido
'* Auth: Franco Rosatti
'* Date: Sep-2019
'* Vers: 1.0
'**********************************************************************

Public Class FormPedido
#Region "INICIO Y FIN"
    Public Sub New(residentes As List(Of Residente), mes As Integer, año As Integer, Optional value As Pedido = Nothing)
        ' This call is required by the designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.

        If value Is Nothing Then
            Me.Pedido = New Pedido
            Me.Text = "Nuevo Pedido"
        Else
            Me.Pedido = value
            Me.Text = "Modificar Pedido"
        End If

        Me.m_Residentes = residentes

        Me.DTP_Fecha.MinDate = New Date(año, mes, 1)
        Me.DTP_Fecha.MaxDate = New Date(año, mes, DateTime.DaysInMonth(año, mes))
        Me.DTP_Desde.MinDate = New Date(año, mes, 1)
        Me.DTP_Desde.MaxDate = New Date(año, mes, DateTime.DaysInMonth(año, mes))
        Me.DTP_Hasta.MinDate = New Date(año, mes, 1)
        Me.DTP_Hasta.MaxDate = New Date(año, mes, DateTime.DaysInMonth(año, mes))
    End Sub
    Private Sub FormPedido_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Cargar()
    End Sub
#End Region
#Region "VARIABLES Y PROPIEDADES"
    Dim m_Residentes As List(Of Residente)
    Public ReadOnly Property Pedido As Pedido
#End Region
#Region "ACCIONES DE LOS USUARIOS"
    Private Sub B_Aceptar_Click(sender As Object, e As EventArgs) Handles B_Aceptar.Click
        Me.Guardar()
    End Sub
    Private Sub CB_Tipo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CB_Tipo.SelectedIndexChanged
        Select Case Me.IndexToTipo(Me.CB_Tipo.SelectedIndex)
            Case Pedido.ETipo.Pedido
                Me.P_Vacaciones.Hide()
                Me.P_Pedido.Show()
            Case Pedido.ETipo.Vacaciones
                Me.P_Pedido.Hide()
                Me.P_Vacaciones.Show()
            Case Else
                Me.P_Vacaciones.Hide()
                Me.P_Pedido.Hide()
        End Select
    End Sub
#End Region
#Region "METODOS PRIVADOS"
    Private Sub Cargar()
        Me.CargarResidentes()
        Me.CB_Tipo.SelectedIndex = Me.TipoToIndex(Me.Pedido.Tipo)
        If Me.Pedido.Tipo = Pedido.ETipo.Pedido Then
            If Me.Pedido.Trabajar Then Me.CB_Pedido.SelectedIndex = 0 Else Me.CB_Pedido.SelectedIndex = 1
            If Me.Pedido.Desde >= Me.DTP_Fecha.MinDate AndAlso Me.Pedido.Desde <= Me.DTP_Fecha.MaxDate Then
                Me.DTP_Fecha.Value = Me.Pedido.Desde
            Else
                Me.DTP_Fecha.Value = Me.DTP_Fecha.MinDate
            End If
        ElseIf Me.Pedido.Tipo = Pedido.ETipo.Vacaciones Then
            If Me.Pedido.Desde >= Me.DTP_Desde.MinDate AndAlso Me.Pedido.Desde <= Me.DTP_Desde.MaxDate Then
                Me.DTP_Desde.Value = Me.Pedido.Desde
            Else
                Me.DTP_Desde.Value = Me.DTP_Desde.MinDate
            End If
            If Me.Pedido.Hasta >= Me.DTP_Hasta.MinDate AndAlso Me.Pedido.Hasta <= Me.DTP_Hasta.MaxDate Then
                Me.DTP_Hasta.Value = Me.Pedido.Hasta
            Else
                Me.DTP_Hasta.Value = Me.DTP_Hasta.MinDate
            End If
        End If
    End Sub
    Private Sub CargarResidentes()
        Me.CB_Residente.BeginUpdate()
        Dim cbi As ComboBoxItem = Nothing
        For Each item As Residente In Me.m_Residentes
            cbi = New ComboBoxItem With {.Nombre = item.Nombre + " " + item.Apellido, .Valor = item}
            Me.CB_Residente.Items.Add(cbi)
            If Me.Pedido.Residente Is item Then Me.CB_Residente.SelectedItem = cbi
        Next
        Me.CB_Residente.EndUpdate()
        Me.CB_Residente.Refresh()
    End Sub
    Private Sub Guardar()
        If Me.Validar() Then
            Me.Pedido.Residente = CType(Me.CB_Residente.SelectedItem, ComboBoxItem).Valor
            Me.Pedido.Tipo = Me.IndexToTipo(Me.CB_Tipo.SelectedIndex)
            If Me.Pedido.Tipo = Pedido.ETipo.Pedido Then
                Me.Pedido.Desde = Me.DTP_Fecha.Value
                Me.Pedido.Hasta = Me.DTP_Fecha.Value
                Me.Pedido.Trabajar = IIf(Me.CB_Pedido.SelectedIndex = 0, True, False)
            ElseIf Me.Pedido.Tipo = Pedido.ETipo.Vacaciones Then
                Me.Pedido.Desde = Me.DTP_Desde.Value
                Me.Pedido.Hasta = Me.DTP_Hasta.Value
                Me.Pedido.Trabajar = False
            Else
                'Unreachable
            End If
            Me.DialogResult = DialogResult.OK
        End If
    End Sub
    Private Function Validar() As Boolean
        Dim msg As String = ""

        If Me.CB_Residente.SelectedIndex < 0 OrElse Me.CB_Residente.SelectedIndex >= Me.CB_Residente.Items.Count Then
            msg += "• Seleccione un residente." + vbCrLf
        End If

        If Me.CB_Tipo.SelectedIndex < 0 OrElse Me.CB_Tipo.SelectedIndex >= Me.CB_Tipo.Items.Count Then
            msg += "• Seleccione si es un pedido o vacaciones." + vbCrLf
        Else
            Select Case Me.IndexToTipo(Me.CB_Tipo.SelectedIndex)
                Case Pedido.ETipo.Pedido
                    If Me.CB_Pedido.SelectedIndex < 0 OrElse Me.CB_Pedido.SelectedIndex >= Me.CB_Pedido.Items.Count Then
                        msg += "• Seleccione el tipo de pedido." + vbCrLf
                    End If

                    If Me.DTP_Fecha.Value = Nothing Then
                        msg += "• Seleccione una fecha." + vbCrLf
                    End If
                Case Pedido.ETipo.Vacaciones
                    If Me.DTP_Desde.Value = Nothing Then
                        msg += "• Seleccione la fecha de inicio de las vacaciones." + vbCrLf
                    End If
                    If Me.DTP_Hasta.Value = Nothing Then
                        msg += "• Seleccione la fecha final de las vacaciones." + vbCrLf
                    End If
                Case Else
                    msg += "• Seleccione si es un pedido o vacaciones." + vbCrLf
            End Select
        End If

        If msg <> "" Then
            MsgBox(msg, Buttons:=MsgBoxStyle.Exclamation Or MsgBoxStyle.OkOnly, Title:=TextoService.AppFullName)
            Return False
        End If
        Return True
    End Function
    Private Function IndexToTipo(index As Integer) As Pedido.ETipo
        Select Case index
            Case 0 : Return Pedido.ETipo.Pedido
            Case 1 : Return Pedido.ETipo.Vacaciones
            Case Else : Return Pedido.ETipo.None
        End Select
    End Function
    Private Function TipoToIndex(value As Pedido.ETipo) As Integer
        Select Case value
            Case Pedido.ETipo.Pedido : Return 0
            Case Pedido.ETipo.Vacaciones : Return 1
            Case Else : Return -1
        End Select
    End Function
#End Region
#Region "CLASES PRIVADAS"
    Private Class ComboBoxItem
        Public Property Nombre As String
        Public Property Valor As Object
    End Class
#End Region
End Class