﻿'**********************************************************************
'* Name: FormGuardia.vb
'* Desc: Fromulario para configurar una guardia
'* Auth: Franco Rosatti
'* Date: Aug-2019
'* Vers: 1.0
'**********************************************************************

Imports System.ComponentModel

Public Class FormGuardiaConfig
#Region "INICIO Y FIN"
    Public Sub New(value As Guardia, nuevaGuardia As Boolean)
        ' This call is required by the designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.

        Me.Guardia = value

        If nuevaGuardia Then
            Me.Text = "Crear Guardia"
        Else
            Me.Text = "Modificar Guardia"
        End If

        Me.NUD_Lunes.Maximum = value.Grupo.Residentes.Count
        Me.NUD_Martes.Maximum = value.Grupo.Residentes.Count
        Me.NUD_Miercoles.Maximum = value.Grupo.Residentes.Count
        Me.NUD_Jueves.Maximum = value.Grupo.Residentes.Count
        Me.NUD_Viernes.Maximum = value.Grupo.Residentes.Count
        Me.NUD_Sabado.Maximum = value.Grupo.Residentes.Count
        Me.NUD_Domingo.Maximum = value.Grupo.Residentes.Count
    End Sub
    Private Sub FormGuardia_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Cargar()
    End Sub
    Private Sub FormGuardiaConfig_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        If Not Me.Guardar() Then e.Cancel = True
    End Sub
#End Region
#Region "VARIABLES Y PROPIEDADES"
    Public ReadOnly Property Guardia As Guardia
#End Region
#Region "ACCIONES DE LOS USUARIOS"
#Region "Dias"
    Private Sub ChB_Lunes_CheckedChanged(sender As Object, e As EventArgs) Handles ChB_Lunes.CheckedChanged
        Me.NUD_Lunes.Enabled = Me.ChB_Lunes.Checked
    End Sub
    Private Sub ChB_Martes_CheckedChanged(sender As Object, e As EventArgs) Handles ChB_Martes.CheckedChanged
        Me.NUD_Martes.Enabled = Me.ChB_Martes.Checked
    End Sub
    Private Sub ChB_Miercoles_CheckedChanged(sender As Object, e As EventArgs) Handles ChB_Miercoles.CheckedChanged
        Me.NUD_Miercoles.Enabled = Me.ChB_Miercoles.Checked
    End Sub
    Private Sub ChB_Jueves_CheckedChanged(sender As Object, e As EventArgs) Handles ChB_Jueves.CheckedChanged
        Me.NUD_Jueves.Enabled = Me.ChB_Jueves.Checked
    End Sub
    Private Sub ChB_Viernes_CheckedChanged(sender As Object, e As EventArgs) Handles ChB_Viernes.CheckedChanged
        Me.NUD_Viernes.Enabled = Me.ChB_Viernes.Checked
    End Sub
    Private Sub ChB_Sabado_CheckedChanged(sender As Object, e As EventArgs) Handles ChB_Sabado.CheckedChanged
        Me.NUD_Sabado.Enabled = Me.ChB_Sabado.Checked
    End Sub
    Private Sub ChB_Domingo_CheckedChanged(sender As Object, e As EventArgs) Handles ChB_Domingo.CheckedChanged
        Me.NUD_Domingo.Enabled = Me.ChB_Domingo.Checked
    End Sub
#End Region
#Region "Pedidos"
    Private Sub TSB_CrearPedido_Click(sender As Object, e As EventArgs) Handles TSB_CrearPedido.Click
        Me.CrearPedido()
    End Sub
    Private Sub TSB_ModificarPedido_Click(sender As Object, e As EventArgs) Handles TSB_ModificarPedido.Click
        Me.ModificarPedido()
    End Sub
    Private Sub TSB_EliminarPedido_Click(sender As Object, e As EventArgs) Handles TSB_EliminarPedido.Click
        Me.EliminarPedido()
    End Sub
    Private Sub LV_Pedidos_DoubleClick(sender As Object, e As EventArgs) Handles LV_Pedidos.DoubleClick
        Me.ModificarPedido()
    End Sub
    Private Sub LV_Pedidos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LV_Pedidos.SelectedIndexChanged
        If Me.LV_Pedidos.SelectedItems.Count = 1 Then
            Me.TSB_ModificarPedido.Enabled = True
            Me.TSB_EliminarPedido.Enabled = True
        Else
            Me.TSB_ModificarPedido.Enabled = False
            Me.TSB_EliminarPedido.Enabled = False
        End If
    End Sub
#End Region
#Region "Excepciones"
    Private Sub TSB_CrearExcepcion_Click(sender As Object, e As EventArgs) Handles TSB_CrearExcepcion.Click
        Me.CrearExcepcion()
    End Sub
    Private Sub TSB_ModificarExcepcion_Click(sender As Object, e As EventArgs) Handles TSB_ModificarExcepcion.Click
        Me.ModificarExcepcion()
    End Sub
    Private Sub TSB_EliminarExcepcion_Click(sender As Object, e As EventArgs) Handles TSB_EliminarExcepcion.Click
        Me.EliminarExcepcion()
    End Sub
    Private Sub LV_Excepciones_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LV_Excepciones.SelectedIndexChanged
        If Me.LV_Excepciones.SelectedItems.Count = 1 Then
            Me.TSB_ModificarExcepcion.Enabled = True
            Me.TSB_EliminarExcepcion.Enabled = True
        Else
            Me.TSB_ModificarExcepcion.Enabled = False
            Me.TSB_EliminarExcepcion.Enabled = False
        End If
    End Sub
    Private Sub LV_Excepciones_DoubleClick(sender As Object, e As EventArgs) Handles LV_Excepciones.DoubleClick
        Me.ModificarExcepcion()
    End Sub
#End Region
#End Region
#Region "METODOS PRIVADOS"
    Private Sub Cargar()
        Me.ChB_Lunes.Checked = Me.Guardia.Lunes
        If Me.Guardia.Lunes Then Me.NUD_Lunes.Value = Math.Max(Me.NUD_Lunes.Minimum, Math.Min(Me.NUD_Lunes.Maximum, Me.Guardia.ResidentesLunes))

        Me.ChB_Martes.Checked = Me.Guardia.Martes
        If Me.Guardia.Martes Then Me.NUD_Martes.Value = Math.Max(Me.NUD_Martes.Minimum, Math.Min(Me.NUD_Martes.Maximum, Me.Guardia.ResidentesMartes))

        Me.ChB_Miercoles.Checked = Me.Guardia.Miercoles
        If Me.Guardia.Miercoles Then Me.NUD_Miercoles.Value = Math.Max(Me.NUD_Miercoles.Minimum, Math.Min(Me.NUD_Miercoles.Maximum, Me.Guardia.ResidentesMiercoles))

        Me.ChB_Jueves.Checked = Me.Guardia.Jueves
        If Me.Guardia.Jueves Then Me.NUD_Jueves.Value = Math.Max(Me.NUD_Jueves.Minimum, Math.Min(Me.NUD_Jueves.Maximum, Me.Guardia.ResidentesJueves))

        Me.ChB_Viernes.Checked = Me.Guardia.Viernes
        If Me.Guardia.Viernes Then Me.NUD_Viernes.Value = Math.Max(Me.NUD_Viernes.Minimum, Math.Min(Me.NUD_Viernes.Maximum, Me.Guardia.ResidentesViernes))

        Me.ChB_Sabado.Checked = Me.Guardia.Sabado
        If Me.Guardia.Sabado Then Me.NUD_Sabado.Value = Math.Max(Me.NUD_Sabado.Minimum, Math.Min(Me.NUD_Sabado.Maximum, Me.Guardia.ResidentesSabado))

        Me.ChB_Domingo.Checked = Me.Guardia.Domingo
        If Me.Guardia.Domingo Then Me.NUD_Domingo.Value = Math.Max(Me.NUD_Domingo.Minimum, Math.Min(Me.NUD_Domingo.Maximum, Me.Guardia.ResidentesDomingo))

        Me.CargarPedidos()
        Me.CargarExcepciones()
    End Sub
    Private Function Guardar() As Boolean
        If Me.Validar() Then
            Me.Guardia.ResidentesLunes = IIf(Me.Guardia.Lunes, Me.NUD_Lunes.Value, 0)
            Me.Guardia.ResidentesMartes = IIf(Me.Guardia.Martes, Me.NUD_Martes.Value, 0)
            Me.Guardia.ResidentesMiercoles = IIf(Me.Guardia.Miercoles, Me.NUD_Miercoles.Value, 0)
            Me.Guardia.ResidentesJueves = IIf(Me.Guardia.Jueves, Me.NUD_Jueves.Value, 0)
            Me.Guardia.ResidentesViernes = IIf(Me.Guardia.Viernes, Me.NUD_Viernes.Value, 0)
            Me.Guardia.ResidentesSabado = IIf(Me.Guardia.Sabado, Me.NUD_Sabado.Value, 0)
            Me.Guardia.ResidentesDomingo = IIf(Me.Guardia.Domingo, Me.NUD_Domingo.Value, 0)

            Me.DialogResult = DialogResult.OK
            Return True
        End If
        Return False
    End Function
    Private Function Validar() As Boolean
        Dim msg As String = ""

        If Me.ChB_Lunes.Checked Then
            If Me.NUD_Lunes.Value <= 0 Then
                msg += "• Debe haber al menos un residente de guardia el día Lunes." + vbCrLf
            End If
        End If
        If Me.ChB_Martes.Checked Then
            If Me.NUD_Martes.Value <= 0 Then
                msg += "• Debe haber al menos un residente de guardia el día Martes." + vbCrLf
            End If
        End If
        If Me.ChB_Miercoles.Checked Then
            If Me.NUD_Miercoles.Value <= 0 Then
                msg += "• Debe haber al menos un residente de guardia el día MIércoles." + vbCrLf
            End If
        End If
        If Me.ChB_Jueves.Checked Then
            If Me.NUD_Jueves.Value <= 0 Then
                msg += "• Debe haber al menos un residente de guardia el día Jueves." + vbCrLf
            End If
        End If
        If Me.ChB_Viernes.Checked Then
            If Me.NUD_Viernes.Value <= 0 Then
                msg += "• Debe haber al menos un residente de guardia el día Viernes." + vbCrLf
            End If
        End If
        If Me.ChB_Sabado.Checked Then
            If Me.NUD_Sabado.Value <= 0 Then
                msg += "• Debe haber al menos un residente de guardia el día Sábado." + vbCrLf
            End If
        End If
        If Me.ChB_Domingo.Checked Then
            If Me.NUD_Domingo.Value <= 0 Then
                msg += "• Debe haber al menos un residente de guardia el día Domingo." + vbCrLf
            End If
        End If
        If msg <> "" Then
            MsgBox(msg, Buttons:=MsgBoxStyle.Exclamation Or MsgBoxStyle.OkOnly, Title:=TextoService.AppFullName)
            Return False
        End If
        Return True
    End Function
#Region "Pedidos"
    Private Sub CargarPedidos()
        Me.LV_Pedidos.BeginUpdate()
        Me.LV_Pedidos.Items.Clear()
        For Each p As Pedido In Me.Guardia.Pedidos
            Me.LV_Pedidos.Items.Add(New PedidoItem(p))
        Next
        Me.LV_Pedidos.EndUpdate()
    End Sub
    Private Sub CrearPedido()
        Dim f As New FormPedido(Me.Guardia.Grupo.Residentes, Me.Guardia.Mes, Me.Guardia.Año)
        If f.ShowDialog() = DialogResult.OK Then
            Me.Guardia.Pedidos.Add(f.Pedido)
            If Not GuardiaData.Save() Then
                Me.Guardia.Pedidos.Remove(f.Pedido)
                MsgBox("No fue posible guardar el pedido.", Buttons:=MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly, Title:=TextoService.AppFullName)
                Return
            End If
            Me.LV_Pedidos.Items.Add(New PedidoItem(f.Pedido))
        End If
    End Sub
    Private Sub ModificarPedido()
        If Me.LV_Pedidos.SelectedItems.Count <> 1 Then
            MsgBox("Seleccione un pedido.", Buttons:=MsgBoxStyle.Exclamation Or MsgBoxStyle.OkOnly, Title:=TextoService.AppFullName)
            Return
        End If

        Dim item As PedidoItem = Me.LV_Pedidos.SelectedItems(0)

        Dim f As New FormPedido(Me.Guardia.Grupo.Residentes, Me.Guardia.Mes, Me.Guardia.Año, item.Pedido)
        If f.ShowDialog() = DialogResult.OK Then
            item.Update()
        End If
    End Sub
    Private Sub EliminarPedido()
        If Me.LV_Pedidos.SelectedItems.Count <> 1 Then
            MsgBox("Seleccione un pedido.", Buttons:=MsgBoxStyle.Exclamation Or MsgBoxStyle.OkOnly, Title:=TextoService.AppFullName)
            Return
        End If

        If MsgBox("¿Está seguro que desea eliminar el pedido?", Buttons:=MsgBoxStyle.Question Or MsgBoxStyle.YesNoCancel, Title:=TextoService.AppFullName) <> MsgBoxResult.Yes Then Return

        Dim item As PedidoItem = Me.LV_Pedidos.SelectedItems(0)

        Me.Guardia.Pedidos.Remove(item.Pedido)
        If Not GuardiaData.Save() Then
            Me.Guardia.Pedidos.Add(item.Pedido)
            MsgBox("No fue posible eliminar el pedido.", Buttons:=MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly, Title:=TextoService.AppFullName)
            Return
        End If

        Me.LV_Pedidos.Items.Remove(item)
    End Sub
#End Region
#Region "Excepciones"
    Private Sub CargarExcepciones()
        Me.LV_Excepciones.BeginUpdate()
        Me.LV_Excepciones.Items.Clear()
        For Each e As Excepcion In Me.Guardia.Excepciones
            Me.LV_Excepciones.Items.Add(New ExcepcionItem(e))
        Next
        Me.LV_Excepciones.EndUpdate()
    End Sub
    Private Sub CrearExcepcion()
        Dim f As New FormExcepcion(Me.Guardia.Mes, Me.Guardia.Año, Me.Guardia.Grupo.Residentes.Count)
        If f.ShowDialog() = DialogResult.OK Then
            Me.Guardia.Excepciones.Add(f.Excepcion)
            If Not GuardiaData.Save() Then
                Me.Guardia.Excepciones.Remove(f.Excepcion)
                MsgBox("No fue posible guardar la excepción.", Buttons:=MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly, Title:=TextoService.AppFullName)
                Return
            End If
            Me.LV_Excepciones.Items.Add(New ExcepcionItem(f.Excepcion))
        End If
    End Sub
    Private Sub ModificarExcepcion()
        If Me.LV_Excepciones.SelectedItems.Count <> 1 Then
            MsgBox("Seleccione una excepción.", Buttons:=MsgBoxStyle.Exclamation Or MsgBoxStyle.OkOnly, Title:=TextoService.AppFullName)
            Return
        End If

        Dim item As ExcepcionItem = Me.LV_Excepciones.SelectedItems(0)

        Dim f As New FormExcepcion(Me.Guardia.Mes, Me.Guardia.Año, Me.Guardia.Grupo.Residentes.Count, item.Excepcion)
        If f.ShowDialog() = DialogResult.OK Then
            item.Update()
        End If
    End Sub
    Private Sub EliminarExcepcion()
        If Me.LV_Excepciones.SelectedItems.Count <> 1 Then
            MsgBox("Seleccione una excepción.", Buttons:=MsgBoxStyle.Exclamation Or MsgBoxStyle.OkOnly, Title:=TextoService.AppFullName)
            Return
        End If

        If MsgBox("¿Está seguro que desea eliminar la excepción?", Buttons:=MsgBoxStyle.Question Or MsgBoxStyle.YesNoCancel, Title:=TextoService.AppFullName) <> MsgBoxResult.Yes Then Return

        Dim item As ExcepcionItem = Me.LV_Excepciones.SelectedItems(0)

        Me.Guardia.Excepciones.Remove(item.Excepcion)
        If Not GuardiaData.Save() Then
            Me.Guardia.Excepciones.Add(item.Excepcion)
            MsgBox("No fue posible eliminar la excepcion.", Buttons:=MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly, Title:=TextoService.AppFullName)
            Return
        End If

        Me.LV_Excepciones.Items.Remove(item)
    End Sub
#End Region
#End Region
#Region "CLASES PRIVADAS"
    Private Class PedidoItem
        Inherits ListViewItem

        Public ReadOnly Property Pedido As Pedido

        Public Sub New(value As Pedido)
            Me.Pedido = value
            Me.Text = Me.Pedido.Residente.NombreCompleto
            If Me.Pedido.Tipo = Pedido.ETipo.Pedido Then
                Me.SubItems.Add(Me.Pedido.Desde.ToString("dd/MM/yyyy"))
            ElseIf Me.Pedido.Tipo = Pedido.ETipo.Vacaciones Then
                Me.SubItems.Add(String.Format("{0} al {1}", Me.Pedido.Desde.ToString("dd/MM/yyyy"), Me.Pedido.Hasta.ToString("dd/MM/yyyy")))
            Else
                Me.SubItems.Add("- - -")
            End If
            Me.SubItems.Add(Me.Pedido.TrabajarString)
        End Sub
        Public Sub Update()
            Me.Text = Me.Pedido.Residente.NombreCompleto
            If Me.Pedido.Tipo = Pedido.ETipo.Pedido Then
                Me.SubItems(1).Text = Me.Pedido.Desde.ToString("dd/MM/yyyy")
            ElseIf Me.Pedido.Tipo = Pedido.ETipo.Vacaciones Then
                Me.SubItems(1).Text = String.Format("{0} al {1}", Me.Pedido.Desde.ToString("dd/MM/yyyy"), Me.Pedido.Hasta.ToString("dd/MM/yyyy"))
            Else
                Me.SubItems(1).Text = "- - -"
            End If
            Me.SubItems(2).Text = Me.Pedido.TrabajarString
        End Sub
    End Class
    Private Class ExcepcionItem
        Inherits ListViewItem

        Public ReadOnly Property Excepcion As Excepcion

        Public Sub New(value As Excepcion)
            Me.Excepcion = value
            Me.Text = Me.Excepcion.Fecha.ToString("dd/MM/yyyy")
            Me.SubItems.Add(Me.Excepcion.LaboralString)
            Me.SubItems.Add(IIf(Me.Excepcion.Laboral, Me.Excepcion.ResidentesDeGuardia.ToString, ""))
        End Sub
        Public Sub Update()
            Me.Text = Me.Excepcion.Fecha.ToString("dd/MM/yyyy")
            Me.SubItems(1).Text = Me.Excepcion.LaboralString
            Me.SubItems(2).Text = IIf(Me.Excepcion.Laboral, Me.Excepcion.ResidentesDeGuardia.ToString, "")
        End Sub
    End Class
#End Region
End Class