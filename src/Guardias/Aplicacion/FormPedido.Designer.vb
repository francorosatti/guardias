﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormPedido
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CB_Residente = New System.Windows.Forms.ComboBox()
        Me.L_Residente = New System.Windows.Forms.Label()
        Me.L_Pedido = New System.Windows.Forms.Label()
        Me.CB_Pedido = New System.Windows.Forms.ComboBox()
        Me.P_Blanco = New System.Windows.Forms.Panel()
        Me.P_Gris = New System.Windows.Forms.Panel()
        Me.B_Aceptar = New System.Windows.Forms.Button()
        Me.B_Cancelar = New System.Windows.Forms.Button()
        Me.L_Fecha = New System.Windows.Forms.Label()
        Me.DTP_Fecha = New System.Windows.Forms.DateTimePicker()
        Me.CB_Tipo = New System.Windows.Forms.ComboBox()
        Me.L_Tipo = New System.Windows.Forms.Label()
        Me.P_Pedido = New System.Windows.Forms.Panel()
        Me.P_Vacaciones = New System.Windows.Forms.Panel()
        Me.L_Hasta = New System.Windows.Forms.Label()
        Me.DTP_Hasta = New System.Windows.Forms.DateTimePicker()
        Me.L_Desde = New System.Windows.Forms.Label()
        Me.DTP_Desde = New System.Windows.Forms.DateTimePicker()
        Me.P_Pedido.SuspendLayout()
        Me.P_Vacaciones.SuspendLayout()
        Me.SuspendLayout()
        '
        'CB_Residente
        '
        Me.CB_Residente.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CB_Residente.DisplayMember = "Nombre"
        Me.CB_Residente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CB_Residente.FormattingEnabled = True
        Me.CB_Residente.Location = New System.Drawing.Point(94, 12)
        Me.CB_Residente.Name = "CB_Residente"
        Me.CB_Residente.Size = New System.Drawing.Size(269, 24)
        Me.CB_Residente.TabIndex = 0
        Me.CB_Residente.ValueMember = "Nombre"
        '
        'L_Residente
        '
        Me.L_Residente.AutoSize = True
        Me.L_Residente.Location = New System.Drawing.Point(12, 15)
        Me.L_Residente.Name = "L_Residente"
        Me.L_Residente.Size = New System.Drawing.Size(76, 17)
        Me.L_Residente.TabIndex = 1
        Me.L_Residente.Text = "Residente:"
        '
        'L_Pedido
        '
        Me.L_Pedido.AutoSize = True
        Me.L_Pedido.Location = New System.Drawing.Point(12, 3)
        Me.L_Pedido.Name = "L_Pedido"
        Me.L_Pedido.Size = New System.Drawing.Size(56, 17)
        Me.L_Pedido.TabIndex = 2
        Me.L_Pedido.Text = "Pedido:"
        '
        'CB_Pedido
        '
        Me.CB_Pedido.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CB_Pedido.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CB_Pedido.FormattingEnabled = True
        Me.CB_Pedido.Items.AddRange(New Object() {"Trabajar", "No Trabajar"})
        Me.CB_Pedido.Location = New System.Drawing.Point(94, 0)
        Me.CB_Pedido.Name = "CB_Pedido"
        Me.CB_Pedido.Size = New System.Drawing.Size(269, 24)
        Me.CB_Pedido.TabIndex = 3
        '
        'P_Blanco
        '
        Me.P_Blanco.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.P_Blanco.BackColor = System.Drawing.SystemColors.Window
        Me.P_Blanco.Location = New System.Drawing.Point(-2, 139)
        Me.P_Blanco.Name = "P_Blanco"
        Me.P_Blanco.Size = New System.Drawing.Size(379, 1)
        Me.P_Blanco.TabIndex = 8
        '
        'P_Gris
        '
        Me.P_Gris.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.P_Gris.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.P_Gris.Location = New System.Drawing.Point(-2, 138)
        Me.P_Gris.Name = "P_Gris"
        Me.P_Gris.Size = New System.Drawing.Size(379, 1)
        Me.P_Gris.TabIndex = 9
        '
        'B_Aceptar
        '
        Me.B_Aceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.B_Aceptar.Location = New System.Drawing.Point(177, 151)
        Me.B_Aceptar.Name = "B_Aceptar"
        Me.B_Aceptar.Size = New System.Drawing.Size(90, 32)
        Me.B_Aceptar.TabIndex = 10
        Me.B_Aceptar.Text = "Aceptar"
        Me.B_Aceptar.UseVisualStyleBackColor = True
        '
        'B_Cancelar
        '
        Me.B_Cancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.B_Cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.B_Cancelar.Location = New System.Drawing.Point(273, 151)
        Me.B_Cancelar.Name = "B_Cancelar"
        Me.B_Cancelar.Size = New System.Drawing.Size(90, 32)
        Me.B_Cancelar.TabIndex = 11
        Me.B_Cancelar.Text = "Cancelar"
        Me.B_Cancelar.UseVisualStyleBackColor = True
        '
        'L_Fecha
        '
        Me.L_Fecha.AutoSize = True
        Me.L_Fecha.Location = New System.Drawing.Point(12, 33)
        Me.L_Fecha.Name = "L_Fecha"
        Me.L_Fecha.Size = New System.Drawing.Size(51, 17)
        Me.L_Fecha.TabIndex = 12
        Me.L_Fecha.Text = "Fecha:"
        '
        'DTP_Fecha
        '
        Me.DTP_Fecha.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DTP_Fecha.CustomFormat = "dd/mm/yyyy"
        Me.DTP_Fecha.Location = New System.Drawing.Point(94, 30)
        Me.DTP_Fecha.Name = "DTP_Fecha"
        Me.DTP_Fecha.Size = New System.Drawing.Size(269, 22)
        Me.DTP_Fecha.TabIndex = 13
        '
        'CB_Tipo
        '
        Me.CB_Tipo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CB_Tipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CB_Tipo.FormattingEnabled = True
        Me.CB_Tipo.Items.AddRange(New Object() {"Pedido", "Vacaciones"})
        Me.CB_Tipo.Location = New System.Drawing.Point(94, 42)
        Me.CB_Tipo.Name = "CB_Tipo"
        Me.CB_Tipo.Size = New System.Drawing.Size(269, 24)
        Me.CB_Tipo.TabIndex = 15
        '
        'L_Tipo
        '
        Me.L_Tipo.AutoSize = True
        Me.L_Tipo.Location = New System.Drawing.Point(12, 45)
        Me.L_Tipo.Name = "L_Tipo"
        Me.L_Tipo.Size = New System.Drawing.Size(40, 17)
        Me.L_Tipo.TabIndex = 14
        Me.L_Tipo.Text = "Tipo:"
        '
        'P_Pedido
        '
        Me.P_Pedido.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.P_Pedido.Controls.Add(Me.CB_Pedido)
        Me.P_Pedido.Controls.Add(Me.L_Pedido)
        Me.P_Pedido.Controls.Add(Me.L_Fecha)
        Me.P_Pedido.Controls.Add(Me.DTP_Fecha)
        Me.P_Pedido.Location = New System.Drawing.Point(0, 72)
        Me.P_Pedido.Name = "P_Pedido"
        Me.P_Pedido.Size = New System.Drawing.Size(375, 52)
        Me.P_Pedido.TabIndex = 16
        Me.P_Pedido.Visible = False
        '
        'P_Vacaciones
        '
        Me.P_Vacaciones.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.P_Vacaciones.Controls.Add(Me.L_Desde)
        Me.P_Vacaciones.Controls.Add(Me.DTP_Desde)
        Me.P_Vacaciones.Controls.Add(Me.L_Hasta)
        Me.P_Vacaciones.Controls.Add(Me.DTP_Hasta)
        Me.P_Vacaciones.Location = New System.Drawing.Point(0, 72)
        Me.P_Vacaciones.Name = "P_Vacaciones"
        Me.P_Vacaciones.Size = New System.Drawing.Size(375, 50)
        Me.P_Vacaciones.TabIndex = 17
        Me.P_Vacaciones.Visible = False
        '
        'L_Hasta
        '
        Me.L_Hasta.AutoSize = True
        Me.L_Hasta.Location = New System.Drawing.Point(12, 31)
        Me.L_Hasta.Name = "L_Hasta"
        Me.L_Hasta.Size = New System.Drawing.Size(49, 17)
        Me.L_Hasta.TabIndex = 12
        Me.L_Hasta.Text = "Hasta:"
        '
        'DTP_Hasta
        '
        Me.DTP_Hasta.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DTP_Hasta.CustomFormat = "dd/mm/yyyy"
        Me.DTP_Hasta.Location = New System.Drawing.Point(94, 28)
        Me.DTP_Hasta.Name = "DTP_Hasta"
        Me.DTP_Hasta.Size = New System.Drawing.Size(269, 22)
        Me.DTP_Hasta.TabIndex = 13
        '
        'L_Desde
        '
        Me.L_Desde.AutoSize = True
        Me.L_Desde.Location = New System.Drawing.Point(12, 3)
        Me.L_Desde.Name = "L_Desde"
        Me.L_Desde.Size = New System.Drawing.Size(53, 17)
        Me.L_Desde.TabIndex = 14
        Me.L_Desde.Text = "Desde:"
        '
        'DTP_Desde
        '
        Me.DTP_Desde.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DTP_Desde.CustomFormat = "dd/mm/yyyy"
        Me.DTP_Desde.Location = New System.Drawing.Point(94, 0)
        Me.DTP_Desde.Name = "DTP_Desde"
        Me.DTP_Desde.Size = New System.Drawing.Size(269, 22)
        Me.DTP_Desde.TabIndex = 15
        '
        'FormPedido
        '
        Me.AcceptButton = Me.B_Aceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(120.0!, 120.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.CancelButton = Me.B_Cancelar
        Me.ClientSize = New System.Drawing.Size(375, 195)
        Me.Controls.Add(Me.P_Vacaciones)
        Me.Controls.Add(Me.P_Pedido)
        Me.Controls.Add(Me.CB_Tipo)
        Me.Controls.Add(Me.L_Tipo)
        Me.Controls.Add(Me.P_Blanco)
        Me.Controls.Add(Me.P_Gris)
        Me.Controls.Add(Me.B_Aceptar)
        Me.Controls.Add(Me.B_Cancelar)
        Me.Controls.Add(Me.L_Residente)
        Me.Controls.Add(Me.CB_Residente)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormPedido"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Pedido"
        Me.P_Pedido.ResumeLayout(False)
        Me.P_Pedido.PerformLayout()
        Me.P_Vacaciones.ResumeLayout(False)
        Me.P_Vacaciones.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents L_Residente As Label
    Private WithEvents L_Pedido As Label
    Private WithEvents P_Blanco As Panel
    Private WithEvents P_Gris As Panel
    Private WithEvents B_Aceptar As Button
    Private WithEvents B_Cancelar As Button
    Private WithEvents CB_Residente As ComboBox
    Private WithEvents CB_Pedido As ComboBox
    Private WithEvents L_Fecha As Label
    Private WithEvents DTP_Fecha As DateTimePicker
    Private WithEvents CB_Tipo As ComboBox
    Private WithEvents L_Tipo As Label
    Private WithEvents P_Pedido As Panel
    Private WithEvents P_Vacaciones As Panel
    Private WithEvents L_Desde As Label
    Private WithEvents DTP_Desde As DateTimePicker
    Private WithEvents L_Hasta As Label
    Private WithEvents DTP_Hasta As DateTimePicker
End Class
