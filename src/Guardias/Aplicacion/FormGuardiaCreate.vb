﻿'**********************************************************************
'* Name: FormGuardiaCreate.vb
'* Desc: Fromulario para crear una guardia
'* Auth: Franco Rosatti
'* Date: Sep-2019
'* Vers: 1.0
'**********************************************************************

Public Class FormGuardiaCreate
#Region "INICIO Y FIN"
    Public Sub New(value As List(Of Guardia))
        ' This call is required by the designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Private Sub FormGuardia_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.CargarGrupos()
        Me.CargarAnios()
    End Sub
#End Region
#Region "VARIABLES Y PROPIEDADES"
    Public ReadOnly Property Guardia As New Guardia
#End Region
#Region "ACCIONES DE LOS USUARIOS"
    Private Sub B_Aceptar_Click(sender As Object, e As EventArgs) Handles B_Aceptar.Click
        Me.Guardar()
    End Sub
#End Region
#Region "METODOS PRIVADOS"
    Private Sub CargarAnios()
        Me.CB_Anio.BeginUpdate()
        Me.CB_Anio.Items.Clear()
        For i As Integer = 2018 To Now.Year + 1
            Me.CB_Anio.Items.Add(i.ToString)
        Next
        If Now.Month = 12 Then
            Me.CB_Anio.SelectedIndex = Me.CB_Anio.Items.Count - 1
            Me.CB_Mes.SelectedIndex = Me.MesToIndex(1)
        Else
            Me.CB_Anio.SelectedIndex = Me.CB_Anio.Items.Count - 2
            Me.CB_Mes.SelectedIndex = Me.MesToIndex(Now.Month + 1)
        End If
        Me.CB_Anio.EndUpdate()
    End Sub
    Private Sub CargarGrupos()
        Me.CB_Grupo.BeginUpdate()
        For Each item As Grupo In ResidenteData.Grupos
            Me.CB_Grupo.Items.Add(New ComboBoxItem With {.Nombre = item.Nombre, .Valor = item})
        Next
        If Me.CB_Grupo.Items.Count = 1 Then Me.CB_Grupo.SelectedIndex = 0
        Me.CB_Grupo.EndUpdate()
        Me.CB_Grupo.Refresh()
    End Sub
    Private Sub Guardar()
        If Me.Validar() Then
            Me.Guardia.Grupo = CType(Me.CB_Grupo.SelectedItem, ComboBoxItem).Valor
            Me.Guardia.Año = Me.CB_Anio.SelectedItem
            Me.Guardia.Mes = Me.IndexToMes(Me.CB_Mes.SelectedIndex)

            Me.DialogResult = DialogResult.OK
        End If
    End Sub
    Private Function Validar() As Boolean
        Dim msg As String = ""

        Dim año As Integer = -1
        Dim mes As Integer = -1
        Dim grupo As Grupo = Nothing

        If Me.CB_Grupo.SelectedIndex < 0 OrElse Me.CB_Grupo.SelectedIndex >= Me.CB_Grupo.Items.Count Then
            msg += "• Seleccione un grupo de residentes." + vbCrLf
        Else
            grupo = CType(Me.CB_Grupo.SelectedItem, ComboBoxItem).Valor
        End If

        If Me.CB_Anio.SelectedIndex < 0 OrElse Me.CB_Anio.SelectedIndex >= Me.CB_Anio.Items.Count Then
            msg += "• Seleccione un año." + vbCrLf
        Else
            año = Me.CB_Anio.SelectedItem
        End If

        If Me.CB_Mes.SelectedIndex < 0 OrElse Me.CB_Mes.SelectedIndex >= Me.CB_Mes.Items.Count Then
            msg += "• Seleccione un mes." + vbCrLf
        Else
            mes = Me.IndexToMes(Me.CB_Mes.SelectedIndex)
        End If

        If año <> -1 AndAlso mes <> -1 AndAlso grupo IsNot Nothing Then
            For Each g As Guardia In GuardiaData.Guardias
                If g.Grupo Is grupo AndAlso g.Año = año AndAlso g.Mes = mes Then
                    msg += String.Format("• Ya existe la guardia de {0} {1} para el grupo {2}", TextoService.MesToString(g.Mes), g.Año.ToString, g.Grupo.Nombre)
                    Exit For
                End If
            Next
        End If

        If msg <> "" Then
            MsgBox(msg, Buttons:=MsgBoxStyle.Exclamation Or MsgBoxStyle.OkOnly, Title:=TextoService.AppFullName)
            Return False
        End If
        Return True
    End Function
    Private Function MesToIndex(mes As Integer) As Integer
        If mes >= 1 AndAlso mes <= 12 Then Return mes - 1
        Return -1
    End Function
    Private Function IndexToMes(idx As Integer) As Integer
        Return Math.Min(12, Math.Max(0, idx + 1))
    End Function
#End Region
#Region "CLASES PRIVADAS"
    Private Class ComboBoxItem
        Public Property Nombre As String
        Public Property Valor As Object
    End Class
#End Region
End Class