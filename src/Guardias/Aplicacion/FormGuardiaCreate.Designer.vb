﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormGuardiaCreate
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormGuardiaCreate))
        Me.L_Grupo = New System.Windows.Forms.Label()
        Me.CB_Grupo = New System.Windows.Forms.ComboBox()
        Me.L_Anio = New System.Windows.Forms.Label()
        Me.CB_Anio = New System.Windows.Forms.ComboBox()
        Me.CB_Mes = New System.Windows.Forms.ComboBox()
        Me.L_Mes = New System.Windows.Forms.Label()
        Me.B_Aceptar = New System.Windows.Forms.Button()
        Me.B_Cancelar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'L_Grupo
        '
        Me.L_Grupo.AutoSize = True
        Me.L_Grupo.Location = New System.Drawing.Point(12, 15)
        Me.L_Grupo.Name = "L_Grupo"
        Me.L_Grupo.Size = New System.Drawing.Size(147, 17)
        Me.L_Grupo.TabIndex = 7
        Me.L_Grupo.Text = "Grupo de Residentes:"
        '
        'CB_Grupo
        '
        Me.CB_Grupo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CB_Grupo.DisplayMember = "Nombre"
        Me.CB_Grupo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CB_Grupo.FormattingEnabled = True
        Me.CB_Grupo.Location = New System.Drawing.Point(165, 12)
        Me.CB_Grupo.Name = "CB_Grupo"
        Me.CB_Grupo.Size = New System.Drawing.Size(257, 24)
        Me.CB_Grupo.TabIndex = 8
        '
        'L_Anio
        '
        Me.L_Anio.AutoSize = True
        Me.L_Anio.Location = New System.Drawing.Point(12, 45)
        Me.L_Anio.Name = "L_Anio"
        Me.L_Anio.Size = New System.Drawing.Size(37, 17)
        Me.L_Anio.TabIndex = 10
        Me.L_Anio.Text = "Año:"
        '
        'CB_Anio
        '
        Me.CB_Anio.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CB_Anio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CB_Anio.FormattingEnabled = True
        Me.CB_Anio.Location = New System.Drawing.Point(165, 42)
        Me.CB_Anio.Name = "CB_Anio"
        Me.CB_Anio.Size = New System.Drawing.Size(257, 24)
        Me.CB_Anio.TabIndex = 11
        '
        'CB_Mes
        '
        Me.CB_Mes.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CB_Mes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CB_Mes.FormattingEnabled = True
        Me.CB_Mes.Items.AddRange(New Object() {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"})
        Me.CB_Mes.Location = New System.Drawing.Point(165, 72)
        Me.CB_Mes.Name = "CB_Mes"
        Me.CB_Mes.Size = New System.Drawing.Size(257, 24)
        Me.CB_Mes.TabIndex = 13
        '
        'L_Mes
        '
        Me.L_Mes.AutoSize = True
        Me.L_Mes.Location = New System.Drawing.Point(12, 78)
        Me.L_Mes.Name = "L_Mes"
        Me.L_Mes.Size = New System.Drawing.Size(38, 17)
        Me.L_Mes.TabIndex = 12
        Me.L_Mes.Text = "Mes:"
        '
        'B_Aceptar
        '
        Me.B_Aceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.B_Aceptar.Location = New System.Drawing.Point(164, 108)
        Me.B_Aceptar.Name = "B_Aceptar"
        Me.B_Aceptar.Size = New System.Drawing.Size(126, 36)
        Me.B_Aceptar.TabIndex = 14
        Me.B_Aceptar.Text = "Aceptar"
        Me.B_Aceptar.UseVisualStyleBackColor = True
        '
        'B_Cancelar
        '
        Me.B_Cancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.B_Cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.B_Cancelar.Location = New System.Drawing.Point(296, 108)
        Me.B_Cancelar.Name = "B_Cancelar"
        Me.B_Cancelar.Size = New System.Drawing.Size(126, 36)
        Me.B_Cancelar.TabIndex = 15
        Me.B_Cancelar.Text = "Cancelar"
        Me.B_Cancelar.UseVisualStyleBackColor = True
        '
        'FormGuardiaCreate
        '
        Me.AcceptButton = Me.B_Aceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(120.0!, 120.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.CancelButton = Me.B_Cancelar
        Me.ClientSize = New System.Drawing.Size(434, 156)
        Me.Controls.Add(Me.B_Cancelar)
        Me.Controls.Add(Me.B_Aceptar)
        Me.Controls.Add(Me.CB_Mes)
        Me.Controls.Add(Me.L_Mes)
        Me.Controls.Add(Me.CB_Anio)
        Me.Controls.Add(Me.L_Anio)
        Me.Controls.Add(Me.CB_Grupo)
        Me.Controls.Add(Me.L_Grupo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormGuardiaCreate"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Nueva Guardia"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents L_Grupo As Label
    Private WithEvents CB_Grupo As ComboBox
    Private WithEvents L_Anio As Label
    Private WithEvents CB_Anio As ComboBox
    Private WithEvents CB_Mes As ComboBox
    Private WithEvents L_Mes As Label
    Private WithEvents B_Cancelar As Button
    Private WithEvents B_Aceptar As Button
End Class
