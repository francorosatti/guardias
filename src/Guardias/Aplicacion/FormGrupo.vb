﻿'**********************************************************************
'* Name: FormGrupo.vb
'* Desc: Formulario para administrar un grupo de residentes
'* Auth: Franco Rosatti
'* Date: Aug-2019
'* Vers: 1.0
'**********************************************************************

Public Class FormGrupo
#Region "INICIO Y FIN"
    Public Sub New(Optional value As Grupo = Nothing)
        ' This call is required by the designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.

        Me.Grupo = value

        If Me.Grupo Is Nothing Then
            Me.Text = "Crear Grupo de Residentes"
            Me.Grupo = New Grupo
        Else
            Me.Text = "Modificar Grupo de Residentes"
        End If
    End Sub
    Private Sub FormGrupo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Cargar()
    End Sub
#End Region
#Region "VARIABLES Y PROPIEDADES"
    Public Property Grupo As Grupo
#End Region
#Region "ACCIONES DE LOS USUARIOS"
    Private Sub B_Guardar_Click(sender As Object, e As EventArgs) Handles B_Guardar.Click
        Me.Guardar()
    End Sub
#Region "List View"
    Private Sub LV_Residentes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LV_Residentes.SelectedIndexChanged
        If Me.LV_Residentes.SelectedItems.Count = 1 Then
            Me.TSB_ModificarResidente.Enabled = True
            Me.TSB_EliminarResidente.Enabled = True
        Else
            Me.TSB_ModificarResidente.Enabled = False
            Me.TSB_EliminarResidente.Enabled = False
        End If
    End Sub
    Private Sub LV_Residentes_DoubleClick(sender As Object, e As EventArgs) Handles LV_Residentes.DoubleClick
        Me.ModificarResidente()
    End Sub
    Private Sub LV_Residentes_ColumnClick(sender As Object, e As ColumnClickEventArgs) Handles LV_Residentes.ColumnClick
        OrdenListas.OrdenarLista(e, Me.LV_Residentes)
    End Sub
#End Region
#Region "ToolBar"
    Private Sub TSB_CrearResidente_Click(sender As Object, e As EventArgs) Handles TSB_CrearResidente.Click
        Me.CrearResidente()
    End Sub
    Private Sub TSB_ModificarResidente_Click(sender As Object, e As EventArgs) Handles TSB_ModificarResidente.Click
        Me.ModificarResidente()
    End Sub
    Private Sub TSB_EliminarResidente_Click(sender As Object, e As EventArgs) Handles TSB_EliminarResidente.Click
        Me.EliminarResidente()
    End Sub
#End Region
#End Region
#Region "METODOS PRIVADOS"
    Private Sub Cargar()
        Me.TB_Nombre.Text = Me.Grupo.Nombre
        Me.LV_Residentes.BeginUpdate()
        For Each r As Residente In Me.Grupo.Residentes
            Me.LV_Residentes.Items.Add(New ResidenteItem(r))
        Next
        Me.LV_Residentes.EndUpdate()
        Me.LV_Residentes.Refresh()
    End Sub
    Private Sub Guardar()
        If Me.Validar Then
            Me.Grupo.Nombre = Me.TB_Nombre.Text

            Me.DialogResult = DialogResult.OK
        End If
    End Sub
    Private Function Validar() As Boolean
        Dim msg As String = ""

        If Me.TB_Nombre.Text Is Nothing OrElse Me.TB_Nombre.Text.Length <= 0 OrElse Me.TB_Nombre.Text = "" Then
            msg += "• Ingrese un nombre" + vbCrLf
        End If

        If msg <> "" Then
            MsgBox(msg, Buttons:=MsgBoxStyle.Exclamation Or MsgBoxStyle.OkOnly, Title:=TextoService.AppFullName)
            Return False
        End If
        Return True
    End Function
    Private Sub CrearResidente()
        Dim f As New FormResidente()
        If f.ShowDialog = DialogResult.OK Then
            Me.Grupo.Residentes.Add(f.Residente)
            Me.LV_Residentes.Items.Add(New ResidenteItem(f.Residente))
        End If
    End Sub
    Private Sub ModificarResidente()
        If Me.LV_Residentes.SelectedItems.Count <> 1 Then
            MsgBox("Seleccione un residente", Buttons:=MsgBoxStyle.Exclamation Or MsgBoxStyle.OkOnly, Title:=TextoService.AppFullName)
            Return
        End If

        Dim item As ResidenteItem = Me.LV_Residentes.SelectedItems(0)
        Dim f As New FormResidente(item.Residente)
        If f.ShowDialog = DialogResult.OK Then
            item.Update()
        End If
    End Sub
    Private Sub EliminarResidente()
        If Me.LV_Residentes.SelectedItems.Count <> 1 Then
            MsgBox("Seleccione un residente", Buttons:=MsgBoxStyle.Exclamation Or MsgBoxStyle.OkOnly, Title:=TextoService.AppFullName)
            Return
        End If

        Dim item As ResidenteItem = Me.LV_Residentes.SelectedItems(0)
        Me.Grupo.Residentes.Remove(item.Residente)
        Me.LV_Residentes.Items.Remove(item)
    End Sub
#End Region
#Region "CLASES PRIVADAS"
    Private Class ResidenteItem
        Inherits ListViewItem

        Public ReadOnly Property Residente As Residente

        Public Sub New(value As Residente)
            Me.Residente = value
            Me.ImageIndex = 0
            Me.text = String.Format("{0} {1}", Me.Residente.Nombre, Me.Residente.Apellido)
        End Sub
        Public Sub Update()
            Me.Text = String.Format("{0} {1}", Me.Residente.Nombre, Me.Residente.Apellido)
        End Sub
    End Class
#End Region
End Class