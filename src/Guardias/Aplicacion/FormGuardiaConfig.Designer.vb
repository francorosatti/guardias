﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormGuardiaConfig
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormGuardiaConfig))
        Me.ChB_Lunes = New System.Windows.Forms.CheckBox()
        Me.ChB_Martes = New System.Windows.Forms.CheckBox()
        Me.ChB_Jueves = New System.Windows.Forms.CheckBox()
        Me.ChB_Miercoles = New System.Windows.Forms.CheckBox()
        Me.ChB_Sabado = New System.Windows.Forms.CheckBox()
        Me.ChB_Viernes = New System.Windows.Forms.CheckBox()
        Me.ChB_Domingo = New System.Windows.Forms.CheckBox()
        Me.GB_Dias = New System.Windows.Forms.GroupBox()
        Me.P_Excepciones = New System.Windows.Forms.Panel()
        Me.LV_Excepciones = New System.Windows.Forms.ListView()
        Me.CH_ExcepcionFecha = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.CH_ExcepcionExcepcion = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.CH_ResidentesGuardia = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TS_Excepciones = New System.Windows.Forms.ToolStrip()
        Me.TSB_CrearExcepcion = New System.Windows.Forms.ToolStripButton()
        Me.TSB_ModificarExcepcion = New System.Windows.Forms.ToolStripButton()
        Me.TSB_EliminarExcepcion = New System.Windows.Forms.ToolStripButton()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.L_Excepciones = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.NUD_Domingo = New System.Windows.Forms.NumericUpDown()
        Me.NUD_Sabado = New System.Windows.Forms.NumericUpDown()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.NUD_Viernes = New System.Windows.Forms.NumericUpDown()
        Me.NUD_Jueves = New System.Windows.Forms.NumericUpDown()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.NUD_Miercoles = New System.Windows.Forms.NumericUpDown()
        Me.NUD_Martes = New System.Windows.Forms.NumericUpDown()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.NUD_Lunes = New System.Windows.Forms.NumericUpDown()
        Me.B_Cerrar = New System.Windows.Forms.Button()
        Me.LV_Pedidos = New System.Windows.Forms.ListView()
        Me.CH_Residente = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.CH_Fecha = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.CH_Pedido = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.P_Pedidos = New System.Windows.Forms.Panel()
        Me.TS_Pedidos = New System.Windows.Forms.ToolStrip()
        Me.TSB_CrearPedido = New System.Windows.Forms.ToolStripButton()
        Me.TSB_ModificarPedido = New System.Windows.Forms.ToolStripButton()
        Me.TSB_EliminarPedido = New System.Windows.Forms.ToolStripButton()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.L_Pedidos = New System.Windows.Forms.Label()
        Me.GB_Dias.SuspendLayout()
        Me.P_Excepciones.SuspendLayout()
        Me.TS_Excepciones.SuspendLayout()
        CType(Me.NUD_Domingo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NUD_Sabado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NUD_Viernes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NUD_Jueves, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NUD_Miercoles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NUD_Martes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NUD_Lunes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.P_Pedidos.SuspendLayout()
        Me.TS_Pedidos.SuspendLayout()
        Me.SuspendLayout()
        '
        'ChB_Lunes
        '
        Me.ChB_Lunes.AutoSize = True
        Me.ChB_Lunes.Location = New System.Drawing.Point(17, 32)
        Me.ChB_Lunes.Name = "ChB_Lunes"
        Me.ChB_Lunes.Size = New System.Drawing.Size(69, 21)
        Me.ChB_Lunes.TabIndex = 0
        Me.ChB_Lunes.Text = "Lunes"
        Me.ChB_Lunes.UseVisualStyleBackColor = True
        '
        'ChB_Martes
        '
        Me.ChB_Martes.AutoSize = True
        Me.ChB_Martes.Location = New System.Drawing.Point(17, 66)
        Me.ChB_Martes.Name = "ChB_Martes"
        Me.ChB_Martes.Size = New System.Drawing.Size(73, 21)
        Me.ChB_Martes.TabIndex = 1
        Me.ChB_Martes.Text = "Martes"
        Me.ChB_Martes.UseVisualStyleBackColor = True
        '
        'ChB_Jueves
        '
        Me.ChB_Jueves.AutoSize = True
        Me.ChB_Jueves.Location = New System.Drawing.Point(17, 134)
        Me.ChB_Jueves.Name = "ChB_Jueves"
        Me.ChB_Jueves.Size = New System.Drawing.Size(75, 21)
        Me.ChB_Jueves.TabIndex = 3
        Me.ChB_Jueves.Text = "Jueves"
        Me.ChB_Jueves.UseVisualStyleBackColor = True
        '
        'ChB_Miercoles
        '
        Me.ChB_Miercoles.AutoSize = True
        Me.ChB_Miercoles.Location = New System.Drawing.Point(17, 100)
        Me.ChB_Miercoles.Name = "ChB_Miercoles"
        Me.ChB_Miercoles.Size = New System.Drawing.Size(90, 21)
        Me.ChB_Miercoles.TabIndex = 2
        Me.ChB_Miercoles.Text = "Miercoles"
        Me.ChB_Miercoles.UseVisualStyleBackColor = True
        '
        'ChB_Sabado
        '
        Me.ChB_Sabado.AutoSize = True
        Me.ChB_Sabado.Location = New System.Drawing.Point(17, 202)
        Me.ChB_Sabado.Name = "ChB_Sabado"
        Me.ChB_Sabado.Size = New System.Drawing.Size(79, 21)
        Me.ChB_Sabado.TabIndex = 5
        Me.ChB_Sabado.Text = "Sábado"
        Me.ChB_Sabado.UseVisualStyleBackColor = True
        '
        'ChB_Viernes
        '
        Me.ChB_Viernes.AutoSize = True
        Me.ChB_Viernes.Location = New System.Drawing.Point(17, 168)
        Me.ChB_Viernes.Name = "ChB_Viernes"
        Me.ChB_Viernes.Size = New System.Drawing.Size(78, 21)
        Me.ChB_Viernes.TabIndex = 4
        Me.ChB_Viernes.Text = "Viernes"
        Me.ChB_Viernes.UseVisualStyleBackColor = True
        '
        'ChB_Domingo
        '
        Me.ChB_Domingo.AutoSize = True
        Me.ChB_Domingo.Location = New System.Drawing.Point(17, 236)
        Me.ChB_Domingo.Name = "ChB_Domingo"
        Me.ChB_Domingo.Size = New System.Drawing.Size(86, 21)
        Me.ChB_Domingo.TabIndex = 6
        Me.ChB_Domingo.Text = "Domingo"
        Me.ChB_Domingo.UseVisualStyleBackColor = True
        '
        'GB_Dias
        '
        Me.GB_Dias.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GB_Dias.Controls.Add(Me.P_Excepciones)
        Me.GB_Dias.Controls.Add(Me.Label8)
        Me.GB_Dias.Controls.Add(Me.NUD_Domingo)
        Me.GB_Dias.Controls.Add(Me.NUD_Sabado)
        Me.GB_Dias.Controls.Add(Me.Label5)
        Me.GB_Dias.Controls.Add(Me.Label6)
        Me.GB_Dias.Controls.Add(Me.NUD_Viernes)
        Me.GB_Dias.Controls.Add(Me.NUD_Jueves)
        Me.GB_Dias.Controls.Add(Me.Label3)
        Me.GB_Dias.Controls.Add(Me.Label4)
        Me.GB_Dias.Controls.Add(Me.NUD_Miercoles)
        Me.GB_Dias.Controls.Add(Me.NUD_Martes)
        Me.GB_Dias.Controls.Add(Me.Label2)
        Me.GB_Dias.Controls.Add(Me.Label1)
        Me.GB_Dias.Controls.Add(Me.NUD_Lunes)
        Me.GB_Dias.Controls.Add(Me.ChB_Lunes)
        Me.GB_Dias.Controls.Add(Me.ChB_Martes)
        Me.GB_Dias.Controls.Add(Me.ChB_Miercoles)
        Me.GB_Dias.Controls.Add(Me.ChB_Domingo)
        Me.GB_Dias.Controls.Add(Me.ChB_Jueves)
        Me.GB_Dias.Controls.Add(Me.ChB_Sabado)
        Me.GB_Dias.Controls.Add(Me.ChB_Viernes)
        Me.GB_Dias.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.GB_Dias.Location = New System.Drawing.Point(12, 12)
        Me.GB_Dias.Name = "GB_Dias"
        Me.GB_Dias.Size = New System.Drawing.Size(1033, 276)
        Me.GB_Dias.TabIndex = 9
        Me.GB_Dias.TabStop = False
        Me.GB_Dias.Text = "Seleccione los días de guardia"
        '
        'P_Excepciones
        '
        Me.P_Excepciones.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.P_Excepciones.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.P_Excepciones.Controls.Add(Me.LV_Excepciones)
        Me.P_Excepciones.Controls.Add(Me.TS_Excepciones)
        Me.P_Excepciones.Controls.Add(Me.Label7)
        Me.P_Excepciones.Controls.Add(Me.L_Excepciones)
        Me.P_Excepciones.Location = New System.Drawing.Point(411, 32)
        Me.P_Excepciones.Name = "P_Excepciones"
        Me.P_Excepciones.Size = New System.Drawing.Size(609, 226)
        Me.P_Excepciones.TabIndex = 18
        '
        'LV_Excepciones
        '
        Me.LV_Excepciones.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.LV_Excepciones.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.CH_ExcepcionFecha, Me.CH_ExcepcionExcepcion, Me.CH_ResidentesGuardia})
        Me.LV_Excepciones.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LV_Excepciones.FullRowSelect = True
        Me.LV_Excepciones.Location = New System.Drawing.Point(0, 65)
        Me.LV_Excepciones.MultiSelect = False
        Me.LV_Excepciones.Name = "LV_Excepciones"
        Me.LV_Excepciones.Size = New System.Drawing.Size(607, 159)
        Me.LV_Excepciones.TabIndex = 16
        Me.LV_Excepciones.UseCompatibleStateImageBehavior = False
        Me.LV_Excepciones.View = System.Windows.Forms.View.Details
        '
        'CH_ExcepcionFecha
        '
        Me.CH_ExcepcionFecha.Text = "Fecha"
        Me.CH_ExcepcionFecha.Width = 180
        '
        'CH_ExcepcionExcepcion
        '
        Me.CH_ExcepcionExcepcion.Text = "Excepción"
        Me.CH_ExcepcionExcepcion.Width = 180
        '
        'CH_ResidentesGuardia
        '
        Me.CH_ResidentesGuardia.Text = "Residentes de Guardia"
        Me.CH_ResidentesGuardia.Width = 180
        '
        'TS_Excepciones
        '
        Me.TS_Excepciones.AutoSize = False
        Me.TS_Excepciones.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.TS_Excepciones.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.TS_Excepciones.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSB_CrearExcepcion, Me.TSB_ModificarExcepcion, Me.TSB_EliminarExcepcion})
        Me.TS_Excepciones.Location = New System.Drawing.Point(0, 25)
        Me.TS_Excepciones.Name = "TS_Excepciones"
        Me.TS_Excepciones.Size = New System.Drawing.Size(607, 40)
        Me.TS_Excepciones.TabIndex = 17
        '
        'TSB_CrearExcepcion
        '
        Me.TSB_CrearExcepcion.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.TSB_CrearExcepcion.Image = CType(resources.GetObject("TSB_CrearExcepcion.Image"), System.Drawing.Image)
        Me.TSB_CrearExcepcion.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_CrearExcepcion.Name = "TSB_CrearExcepcion"
        Me.TSB_CrearExcepcion.Size = New System.Drawing.Size(48, 37)
        Me.TSB_CrearExcepcion.Text = "Crear"
        '
        'TSB_ModificarExcepcion
        '
        Me.TSB_ModificarExcepcion.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.TSB_ModificarExcepcion.Enabled = False
        Me.TSB_ModificarExcepcion.Image = CType(resources.GetObject("TSB_ModificarExcepcion.Image"), System.Drawing.Image)
        Me.TSB_ModificarExcepcion.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_ModificarExcepcion.Name = "TSB_ModificarExcepcion"
        Me.TSB_ModificarExcepcion.Size = New System.Drawing.Size(77, 37)
        Me.TSB_ModificarExcepcion.Text = "Modificar"
        '
        'TSB_EliminarExcepcion
        '
        Me.TSB_EliminarExcepcion.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.TSB_EliminarExcepcion.Enabled = False
        Me.TSB_EliminarExcepcion.Image = CType(resources.GetObject("TSB_EliminarExcepcion.Image"), System.Drawing.Image)
        Me.TSB_EliminarExcepcion.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_EliminarExcepcion.Name = "TSB_EliminarExcepcion"
        Me.TSB_EliminarExcepcion.Size = New System.Drawing.Size(67, 37)
        Me.TSB_EliminarExcepcion.Text = "Eliminar"
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.SystemColors.WindowText
        Me.Label7.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.MenuText
        Me.Label7.Location = New System.Drawing.Point(0, 24)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(607, 1)
        Me.Label7.TabIndex = 19
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'L_Excepciones
        '
        Me.L_Excepciones.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.L_Excepciones.Dock = System.Windows.Forms.DockStyle.Top
        Me.L_Excepciones.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.L_Excepciones.ForeColor = System.Drawing.SystemColors.MenuText
        Me.L_Excepciones.Location = New System.Drawing.Point(0, 0)
        Me.L_Excepciones.Name = "L_Excepciones"
        Me.L_Excepciones.Size = New System.Drawing.Size(607, 24)
        Me.L_Excepciones.TabIndex = 18
        Me.L_Excepciones.Text = "FERIADOS Y EXCEPCIONES"
        Me.L_Excepciones.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(156, 237)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(155, 17)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "Residentes de guardia:"
        '
        'NUD_Domingo
        '
        Me.NUD_Domingo.Enabled = False
        Me.NUD_Domingo.Location = New System.Drawing.Point(317, 236)
        Me.NUD_Domingo.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.NUD_Domingo.Name = "NUD_Domingo"
        Me.NUD_Domingo.Size = New System.Drawing.Size(66, 22)
        Me.NUD_Domingo.TabIndex = 19
        '
        'NUD_Sabado
        '
        Me.NUD_Sabado.Enabled = False
        Me.NUD_Sabado.Location = New System.Drawing.Point(317, 202)
        Me.NUD_Sabado.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.NUD_Sabado.Name = "NUD_Sabado"
        Me.NUD_Sabado.Size = New System.Drawing.Size(66, 22)
        Me.NUD_Sabado.TabIndex = 18
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(156, 203)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(155, 17)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "Residentes de guardia:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(156, 169)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(155, 17)
        Me.Label6.TabIndex = 16
        Me.Label6.Text = "Residentes de guardia:"
        '
        'NUD_Viernes
        '
        Me.NUD_Viernes.Enabled = False
        Me.NUD_Viernes.Location = New System.Drawing.Point(317, 168)
        Me.NUD_Viernes.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.NUD_Viernes.Name = "NUD_Viernes"
        Me.NUD_Viernes.Size = New System.Drawing.Size(66, 22)
        Me.NUD_Viernes.TabIndex = 15
        '
        'NUD_Jueves
        '
        Me.NUD_Jueves.Enabled = False
        Me.NUD_Jueves.Location = New System.Drawing.Point(317, 134)
        Me.NUD_Jueves.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.NUD_Jueves.Name = "NUD_Jueves"
        Me.NUD_Jueves.Size = New System.Drawing.Size(66, 22)
        Me.NUD_Jueves.TabIndex = 14
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(156, 135)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(155, 17)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "Residentes de guardia:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(156, 101)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(155, 17)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "Residentes de guardia:"
        '
        'NUD_Miercoles
        '
        Me.NUD_Miercoles.Enabled = False
        Me.NUD_Miercoles.Location = New System.Drawing.Point(317, 100)
        Me.NUD_Miercoles.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.NUD_Miercoles.Name = "NUD_Miercoles"
        Me.NUD_Miercoles.Size = New System.Drawing.Size(66, 22)
        Me.NUD_Miercoles.TabIndex = 11
        '
        'NUD_Martes
        '
        Me.NUD_Martes.Enabled = False
        Me.NUD_Martes.Location = New System.Drawing.Point(317, 66)
        Me.NUD_Martes.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.NUD_Martes.Name = "NUD_Martes"
        Me.NUD_Martes.Size = New System.Drawing.Size(66, 22)
        Me.NUD_Martes.TabIndex = 10
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(156, 67)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(155, 17)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Residentes de guardia:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(156, 33)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(155, 17)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Residentes de guardia:"
        '
        'NUD_Lunes
        '
        Me.NUD_Lunes.Enabled = False
        Me.NUD_Lunes.Location = New System.Drawing.Point(317, 32)
        Me.NUD_Lunes.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.NUD_Lunes.Name = "NUD_Lunes"
        Me.NUD_Lunes.Size = New System.Drawing.Size(66, 22)
        Me.NUD_Lunes.TabIndex = 7
        '
        'B_Cerrar
        '
        Me.B_Cerrar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.B_Cerrar.Location = New System.Drawing.Point(919, 695)
        Me.B_Cerrar.Name = "B_Cerrar"
        Me.B_Cerrar.Size = New System.Drawing.Size(126, 36)
        Me.B_Cerrar.TabIndex = 15
        Me.B_Cerrar.Text = "&Cerrar"
        Me.B_Cerrar.UseVisualStyleBackColor = True
        '
        'LV_Pedidos
        '
        Me.LV_Pedidos.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.LV_Pedidos.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.CH_Residente, Me.CH_Fecha, Me.CH_Pedido})
        Me.LV_Pedidos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LV_Pedidos.FullRowSelect = True
        Me.LV_Pedidos.Location = New System.Drawing.Point(0, 65)
        Me.LV_Pedidos.MultiSelect = False
        Me.LV_Pedidos.Name = "LV_Pedidos"
        Me.LV_Pedidos.Size = New System.Drawing.Size(1031, 321)
        Me.LV_Pedidos.TabIndex = 16
        Me.LV_Pedidos.UseCompatibleStateImageBehavior = False
        Me.LV_Pedidos.View = System.Windows.Forms.View.Details
        '
        'CH_Residente
        '
        Me.CH_Residente.Text = "Residente"
        Me.CH_Residente.Width = 300
        '
        'CH_Fecha
        '
        Me.CH_Fecha.Text = "Fecha"
        Me.CH_Fecha.Width = 240
        '
        'CH_Pedido
        '
        Me.CH_Pedido.Text = "Pedido"
        Me.CH_Pedido.Width = 180
        '
        'P_Pedidos
        '
        Me.P_Pedidos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.P_Pedidos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.P_Pedidos.Controls.Add(Me.LV_Pedidos)
        Me.P_Pedidos.Controls.Add(Me.TS_Pedidos)
        Me.P_Pedidos.Controls.Add(Me.Panel1)
        Me.P_Pedidos.Controls.Add(Me.L_Pedidos)
        Me.P_Pedidos.Location = New System.Drawing.Point(12, 294)
        Me.P_Pedidos.Name = "P_Pedidos"
        Me.P_Pedidos.Size = New System.Drawing.Size(1033, 388)
        Me.P_Pedidos.TabIndex = 17
        '
        'TS_Pedidos
        '
        Me.TS_Pedidos.AutoSize = False
        Me.TS_Pedidos.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.TS_Pedidos.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.TS_Pedidos.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSB_CrearPedido, Me.TSB_ModificarPedido, Me.TSB_EliminarPedido})
        Me.TS_Pedidos.Location = New System.Drawing.Point(0, 25)
        Me.TS_Pedidos.Name = "TS_Pedidos"
        Me.TS_Pedidos.Size = New System.Drawing.Size(1031, 40)
        Me.TS_Pedidos.TabIndex = 17
        '
        'TSB_CrearPedido
        '
        Me.TSB_CrearPedido.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.TSB_CrearPedido.Image = CType(resources.GetObject("TSB_CrearPedido.Image"), System.Drawing.Image)
        Me.TSB_CrearPedido.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_CrearPedido.Name = "TSB_CrearPedido"
        Me.TSB_CrearPedido.Size = New System.Drawing.Size(48, 37)
        Me.TSB_CrearPedido.Text = "Crear"
        '
        'TSB_ModificarPedido
        '
        Me.TSB_ModificarPedido.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.TSB_ModificarPedido.Enabled = False
        Me.TSB_ModificarPedido.Image = CType(resources.GetObject("TSB_ModificarPedido.Image"), System.Drawing.Image)
        Me.TSB_ModificarPedido.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_ModificarPedido.Name = "TSB_ModificarPedido"
        Me.TSB_ModificarPedido.Size = New System.Drawing.Size(77, 37)
        Me.TSB_ModificarPedido.Text = "Modificar"
        '
        'TSB_EliminarPedido
        '
        Me.TSB_EliminarPedido.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.TSB_EliminarPedido.Enabled = False
        Me.TSB_EliminarPedido.Image = CType(resources.GetObject("TSB_EliminarPedido.Image"), System.Drawing.Image)
        Me.TSB_EliminarPedido.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_EliminarPedido.Name = "TSB_EliminarPedido"
        Me.TSB_EliminarPedido.Size = New System.Drawing.Size(67, 37)
        Me.TSB_EliminarPedido.Text = "Eliminar"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.WindowText
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 24)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1031, 1)
        Me.Panel1.TabIndex = 18
        '
        'L_Pedidos
        '
        Me.L_Pedidos.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.L_Pedidos.Dock = System.Windows.Forms.DockStyle.Top
        Me.L_Pedidos.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.L_Pedidos.ForeColor = System.Drawing.SystemColors.MenuText
        Me.L_Pedidos.Location = New System.Drawing.Point(0, 0)
        Me.L_Pedidos.Name = "L_Pedidos"
        Me.L_Pedidos.Size = New System.Drawing.Size(1031, 24)
        Me.L_Pedidos.TabIndex = 19
        Me.L_Pedidos.Text = "PEDIDOS Y VACACIONES"
        Me.L_Pedidos.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FormGuardiaConfig
        '
        Me.AcceptButton = Me.B_Cerrar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(120.0!, 120.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.CancelButton = Me.B_Cerrar
        Me.ClientSize = New System.Drawing.Size(1057, 743)
        Me.Controls.Add(Me.P_Pedidos)
        Me.Controls.Add(Me.B_Cerrar)
        Me.Controls.Add(Me.GB_Dias)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormGuardiaConfig"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Guardia"
        Me.GB_Dias.ResumeLayout(False)
        Me.GB_Dias.PerformLayout()
        Me.P_Excepciones.ResumeLayout(False)
        Me.TS_Excepciones.ResumeLayout(False)
        Me.TS_Excepciones.PerformLayout()
        CType(Me.NUD_Domingo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NUD_Sabado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NUD_Viernes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NUD_Jueves, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NUD_Miercoles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NUD_Martes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NUD_Lunes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.P_Pedidos.ResumeLayout(False)
        Me.TS_Pedidos.ResumeLayout(False)
        Me.TS_Pedidos.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents ChB_Lunes As CheckBox
    Private WithEvents ChB_Martes As CheckBox
    Private WithEvents ChB_Jueves As CheckBox
    Private WithEvents ChB_Miercoles As CheckBox
    Private WithEvents ChB_Sabado As CheckBox
    Private WithEvents ChB_Viernes As CheckBox
    Private WithEvents ChB_Domingo As CheckBox
    Private WithEvents GB_Dias As GroupBox
    Private WithEvents B_Cerrar As Button
    Private WithEvents Label8 As Label
    Private WithEvents NUD_Domingo As NumericUpDown
    Private WithEvents NUD_Sabado As NumericUpDown
    Private WithEvents Label5 As Label
    Private WithEvents Label6 As Label
    Private WithEvents NUD_Viernes As NumericUpDown
    Private WithEvents NUD_Jueves As NumericUpDown
    Private WithEvents Label3 As Label
    Private WithEvents Label4 As Label
    Private WithEvents NUD_Miercoles As NumericUpDown
    Private WithEvents Label2 As Label
    Private WithEvents Label1 As Label
    Private WithEvents NUD_Lunes As NumericUpDown
    Private WithEvents NUD_Martes As NumericUpDown
    Private WithEvents LV_Pedidos As ListView
    Private WithEvents CH_Residente As ColumnHeader
    Private WithEvents CH_Fecha As ColumnHeader
    Private WithEvents CH_Pedido As ColumnHeader
    Private WithEvents P_Pedidos As Panel
    Private WithEvents TS_Pedidos As ToolStrip
    Private WithEvents TSB_CrearPedido As ToolStripButton
    Private WithEvents TSB_EliminarPedido As ToolStripButton
    Private WithEvents TSB_ModificarPedido As ToolStripButton
    Private WithEvents P_Excepciones As Panel
    Private WithEvents LV_Excepciones As ListView
    Private WithEvents CH_ExcepcionFecha As ColumnHeader
    Private WithEvents CH_ExcepcionExcepcion As ColumnHeader
    Private WithEvents TS_Excepciones As ToolStrip
    Private WithEvents TSB_CrearExcepcion As ToolStripButton
    Private WithEvents TSB_ModificarExcepcion As ToolStripButton
    Private WithEvents TSB_EliminarExcepcion As ToolStripButton
    Private WithEvents Label7 As Label
    Private WithEvents L_Excepciones As Label
    Private WithEvents Panel1 As Panel
    Private WithEvents L_Pedidos As Label
    Private WithEvents CH_ResidentesGuardia As ColumnHeader
End Class
