﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormGrupos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormGrupos))
        Me.TS_Residentes = New System.Windows.Forms.ToolStrip()
        Me.TSB_CrearGrupo = New System.Windows.Forms.ToolStripButton()
        Me.TSB_ModificarGrupo = New System.Windows.Forms.ToolStripButton()
        Me.TSB_EliminarGrupo = New System.Windows.Forms.ToolStripButton()
        Me.LV_Grupos = New System.Windows.Forms.ListView()
        Me.B_Cerrar = New System.Windows.Forms.Button()
        Me.IL_Grupos = New System.Windows.Forms.ImageList(Me.components)
        Me.TS_Residentes.SuspendLayout()
        Me.SuspendLayout()
        '
        'TS_Residentes
        '
        Me.TS_Residentes.AutoSize = False
        Me.TS_Residentes.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.TS_Residentes.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.TS_Residentes.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSB_CrearGrupo, Me.TSB_ModificarGrupo, Me.TSB_EliminarGrupo})
        Me.TS_Residentes.Location = New System.Drawing.Point(0, 0)
        Me.TS_Residentes.Name = "TS_Residentes"
        Me.TS_Residentes.Size = New System.Drawing.Size(582, 40)
        Me.TS_Residentes.TabIndex = 0
        '
        'TSB_CrearGrupo
        '
        Me.TSB_CrearGrupo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.TSB_CrearGrupo.Image = CType(resources.GetObject("TSB_CrearGrupo.Image"), System.Drawing.Image)
        Me.TSB_CrearGrupo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_CrearGrupo.Name = "TSB_CrearGrupo"
        Me.TSB_CrearGrupo.Size = New System.Drawing.Size(48, 37)
        Me.TSB_CrearGrupo.Text = "Crear"
        '
        'TSB_ModificarGrupo
        '
        Me.TSB_ModificarGrupo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.TSB_ModificarGrupo.Enabled = False
        Me.TSB_ModificarGrupo.Image = CType(resources.GetObject("TSB_ModificarGrupo.Image"), System.Drawing.Image)
        Me.TSB_ModificarGrupo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_ModificarGrupo.Name = "TSB_ModificarGrupo"
        Me.TSB_ModificarGrupo.Size = New System.Drawing.Size(77, 37)
        Me.TSB_ModificarGrupo.Text = "Modificar"
        '
        'TSB_EliminarGrupo
        '
        Me.TSB_EliminarGrupo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.TSB_EliminarGrupo.Enabled = False
        Me.TSB_EliminarGrupo.Image = CType(resources.GetObject("TSB_EliminarGrupo.Image"), System.Drawing.Image)
        Me.TSB_EliminarGrupo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_EliminarGrupo.Name = "TSB_EliminarGrupo"
        Me.TSB_EliminarGrupo.Size = New System.Drawing.Size(67, 37)
        Me.TSB_EliminarGrupo.Text = "Eliminar"
        '
        'LV_Grupos
        '
        Me.LV_Grupos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LV_Grupos.LargeImageList = Me.IL_Grupos
        Me.LV_Grupos.Location = New System.Drawing.Point(0, 43)
        Me.LV_Grupos.MultiSelect = False
        Me.LV_Grupos.Name = "LV_Grupos"
        Me.LV_Grupos.Size = New System.Drawing.Size(582, 250)
        Me.LV_Grupos.TabIndex = 1
        Me.LV_Grupos.UseCompatibleStateImageBehavior = False
        '
        'B_Cerrar
        '
        Me.B_Cerrar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.B_Cerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.B_Cerrar.Location = New System.Drawing.Point(460, 305)
        Me.B_Cerrar.Name = "B_Cerrar"
        Me.B_Cerrar.Size = New System.Drawing.Size(110, 36)
        Me.B_Cerrar.TabIndex = 2
        Me.B_Cerrar.Text = "Cerrar"
        Me.B_Cerrar.UseVisualStyleBackColor = True
        '
        'IL_Grupos
        '
        Me.IL_Grupos.ImageStream = CType(resources.GetObject("IL_Grupos.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.IL_Grupos.TransparentColor = System.Drawing.Color.Transparent
        Me.IL_Grupos.Images.SetKeyName(0, "group.ico")
        '
        'FormGrupos
        '
        Me.AcceptButton = Me.B_Cerrar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(120.0!, 120.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.CancelButton = Me.B_Cerrar
        Me.ClientSize = New System.Drawing.Size(582, 353)
        Me.Controls.Add(Me.B_Cerrar)
        Me.Controls.Add(Me.LV_Grupos)
        Me.Controls.Add(Me.TS_Residentes)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(600, 400)
        Me.Name = "FormGrupos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Grupos de Residentes"
        Me.TS_Residentes.ResumeLayout(False)
        Me.TS_Residentes.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents TS_Residentes As ToolStrip
    Private WithEvents TSB_CrearGrupo As ToolStripButton
    Private WithEvents TSB_ModificarGrupo As ToolStripButton
    Private WithEvents TSB_EliminarGrupo As ToolStripButton
    Private WithEvents LV_Grupos As ListView
    Private WithEvents B_Cerrar As Button
    Private WithEvents IL_Grupos As ImageList
End Class
