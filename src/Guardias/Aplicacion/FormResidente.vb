﻿'**********************************************************************
'* Name: FormResidente.vb
'* Desc: Formulario para administrar un residente
'* Auth: Franco Rosatti
'* Date: Aug-2019
'* Vers: 1.0
'**********************************************************************

Public Class FormResidente
#Region "INICIO Y FIN"
    Public Sub New(Optional value As Residente = Nothing)
        ' This call is required by the designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.
        Me.Residente = value

        If Me.Residente Is Nothing Then
            Me.Text = "Crear Residente"
            Me.Residente = New Residente
        Else
            Me.Text = "Modificar Residente"
        End If
    End Sub
    Private Sub FormResidente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Cargar()
    End Sub
#End Region
#Region "VARIABLES Y PROPIEDADES"
    Public Property Residente As Residente
#End Region
#Region "ACCIONES DE LOS USUARIOS"
    Private Sub B_Aceptar_Click(sender As Object, e As EventArgs) Handles B_Aceptar.Click
        Me.Guardar()
    End Sub
#End Region
#Region "METODOS PRIVADOS"
    Private Sub Cargar()
        Me.TB_Nombre.Text = Me.Residente.Nombre
        Me.TB_Apellido.Text = Me.Residente.Apellido
    End Sub
    Private Sub Guardar()
        If Me.Validar() Then
            Me.Residente.Nombre = Me.TB_Nombre.Text
            Me.Residente.Apellido = Me.TB_Apellido.Text

            Me.DialogResult = DialogResult.OK
        End If
    End Sub
    Private Function Validar() As Boolean
        Dim msg As String = ""

        If Me.TB_Nombre.Text Is Nothing OrElse Me.TB_Nombre.Text.Length <= 0 OrElse Me.TB_Nombre.Text = "" Then
            msg += "• Ingrese un nombre" + vbCrLf
        End If

        If Me.TB_Apellido.Text Is Nothing OrElse Me.TB_Apellido.Text.Length <= 0 OrElse Me.TB_Apellido.Text = "" Then
            msg += "• Ingrese un apellido" + vbCrLf
        End If

        If msg <> "" Then
            MsgBox(msg, Buttons:=MsgBoxStyle.Exclamation Or MsgBoxStyle.OkOnly, Title:=TextoService.AppFullName)
            Return False
        End If
        Return True
    End Function
#End Region
End Class