﻿'**********************************************************************
'* Name: FormGrupos.vb
'* Desc: Fromulario para administrar grupos de residentes
'* Auth: Franco Rosatti
'* Date: Aug-2019
'* Vers: 1.0
'**********************************************************************

Public Class FormGrupos
#Region "INICIO Y FIN"
    Private Sub FormGrupos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.CargarGrupos()
    End Sub
#End Region
#Region "VARIABLES Y PROPIEDADES"

#End Region
#Region "ACCIONES DE LOS USUARIOS"
#Region "List View"
    Private Sub LV_Grupos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LV_Grupos.SelectedIndexChanged
        If Me.LV_Grupos.SelectedItems.Count = 1 Then
            TSB_EliminarGrupo.Enabled = True
            TSB_ModificarGrupo.Enabled = True
        Else
            TSB_EliminarGrupo.Enabled = False
            TSB_ModificarGrupo.Enabled = False
        End If
    End Sub
    Private Sub LV_Grupos_DoubleClick(sender As Object, e As EventArgs) Handles LV_Grupos.DoubleClick
        Me.ModificarGrupo()
    End Sub
    Private Sub LV_Grupos_ColumnClick(sender As Object, e As ColumnClickEventArgs) Handles LV_Grupos.ColumnClick
        OrdenListas.OrdenarLista(e, Me.LV_Grupos)
    End Sub
#End Region
#Region "ToolBar"
    Private Sub TSB_CrearGrupo_Click(sender As Object, e As EventArgs) Handles TSB_CrearGrupo.Click
        Me.CrearGrupo()
    End Sub
    Private Sub TSB_ModificarGrupo_Click(sender As Object, e As EventArgs) Handles TSB_ModificarGrupo.Click
        Me.ModificarGrupo()
    End Sub
    Private Sub TSB_EliminarGrupo_Click(sender As Object, e As EventArgs) Handles TSB_EliminarGrupo.Click
        Me.EliminarGrupo()
    End Sub
#End Region
#End Region
#Region "METODOS PRIVADOS"
    Private Sub CargarGrupos()
        Me.LV_Grupos.BeginUpdate()
        For Each item As Grupo In ResidenteData.Grupos
            Me.LV_Grupos.Items.Add(New GrupoItem(item))
        Next
        Me.LV_Grupos.EndUpdate()
        Me.LV_Grupos.Refresh()
    End Sub
    Private Sub CrearGrupo()
        Dim f As New FormGrupo()
        If f.ShowDialog = DialogResult.OK Then
            ResidenteData.Grupos.Add(f.Grupo)
            If Not ResidenteData.Save() Then
                ResidenteData.Grupos.Remove(f.Grupo)
                MsgBox("No fue posible crear el grupo de residentes", Buttons:=MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly, Title:=TextoService.AppFullName)
                Return
            End If
            Me.LV_Grupos.Items.Add(New GrupoItem(f.Grupo))
        End If
    End Sub
    Private Sub ModificarGrupo()
        If Me.LV_Grupos.SelectedItems.Count <> 1 Then
            MsgBox("Seleccione un grupo de residentes", Buttons:=MsgBoxStyle.Exclamation Or MsgBoxStyle.OkOnly, Title:=TextoService.AppFullName)
            Return
        End If

        Dim item As GrupoItem = Me.LV_Grupos.SelectedItems(0)
        Dim f As New FormGrupo(item.Grupo.Clone)
        If f.ShowDialog = DialogResult.OK Then
            item.Grupo.UnClone(f.Grupo)
            If Not ResidenteData.Save() Then
                MsgBox("No fue posible modificar el grupo de residentes", Buttons:=MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly, Title:=TextoService.AppFullName)
                Return
            End If
            item.Update()
        End If
    End Sub
    Private Sub EliminarGrupo()
        If Me.LV_Grupos.SelectedItems.Count <> 1 Then
            MsgBox("Seleccione un grupo de residentes", Buttons:=MsgBoxStyle.Exclamation Or MsgBoxStyle.OkOnly, Title:=TextoService.AppFullName)
            Return
        End If

        Dim item As GrupoItem = Me.LV_Grupos.SelectedItems(0)
        ResidenteData.Grupos.Remove(item.Grupo)
        If ResidenteData.Save() Then
            ResidenteData.Grupos.Add(item.Grupo)
            Return
        End If
        Me.LV_Grupos.Items.Remove(item)
    End Sub
#End Region
#Region "CLASES PRIVADAS"
    Private Class GrupoItem
        Inherits ListViewItem

        Public ReadOnly Property Grupo As Grupo

        Public Sub New(value As Grupo)
            Me.Grupo = value
            Me.Text = Me.Grupo.Nombre
            Me.ImageIndex = 0
        End Sub

        Public Sub Update()
            Me.Text = Me.Grupo.Nombre
        End Sub
    End Class
#End Region
End Class