﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormExcepcion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.L_Pedido = New System.Windows.Forms.Label()
        Me.CB_Excepcion = New System.Windows.Forms.ComboBox()
        Me.P_Blanco = New System.Windows.Forms.Panel()
        Me.P_Gris = New System.Windows.Forms.Panel()
        Me.B_Aceptar = New System.Windows.Forms.Button()
        Me.B_Cancelar = New System.Windows.Forms.Button()
        Me.L_Fecha = New System.Windows.Forms.Label()
        Me.DTP_Fecha = New System.Windows.Forms.DateTimePicker()
        Me.NUD_Residentes = New System.Windows.Forms.NumericUpDown()
        Me.L_Residentes = New System.Windows.Forms.Label()
        CType(Me.NUD_Residentes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'L_Pedido
        '
        Me.L_Pedido.AutoSize = True
        Me.L_Pedido.Location = New System.Drawing.Point(12, 43)
        Me.L_Pedido.Name = "L_Pedido"
        Me.L_Pedido.Size = New System.Drawing.Size(76, 17)
        Me.L_Pedido.TabIndex = 2
        Me.L_Pedido.Text = "Excepción:"
        '
        'CB_Excepcion
        '
        Me.CB_Excepcion.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CB_Excepcion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CB_Excepcion.FormattingEnabled = True
        Me.CB_Excepcion.Items.AddRange(New Object() {"Laboral", "No Laboral"})
        Me.CB_Excepcion.Location = New System.Drawing.Point(94, 40)
        Me.CB_Excepcion.Name = "CB_Excepcion"
        Me.CB_Excepcion.Size = New System.Drawing.Size(269, 24)
        Me.CB_Excepcion.TabIndex = 3
        '
        'P_Blanco
        '
        Me.P_Blanco.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.P_Blanco.BackColor = System.Drawing.SystemColors.Window
        Me.P_Blanco.Location = New System.Drawing.Point(-2, 105)
        Me.P_Blanco.Name = "P_Blanco"
        Me.P_Blanco.Size = New System.Drawing.Size(379, 1)
        Me.P_Blanco.TabIndex = 8
        '
        'P_Gris
        '
        Me.P_Gris.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.P_Gris.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.P_Gris.Location = New System.Drawing.Point(-2, 104)
        Me.P_Gris.Name = "P_Gris"
        Me.P_Gris.Size = New System.Drawing.Size(379, 1)
        Me.P_Gris.TabIndex = 9
        '
        'B_Aceptar
        '
        Me.B_Aceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.B_Aceptar.Location = New System.Drawing.Point(177, 117)
        Me.B_Aceptar.Name = "B_Aceptar"
        Me.B_Aceptar.Size = New System.Drawing.Size(90, 32)
        Me.B_Aceptar.TabIndex = 10
        Me.B_Aceptar.Text = "Aceptar"
        Me.B_Aceptar.UseVisualStyleBackColor = True
        '
        'B_Cancelar
        '
        Me.B_Cancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.B_Cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.B_Cancelar.Location = New System.Drawing.Point(273, 117)
        Me.B_Cancelar.Name = "B_Cancelar"
        Me.B_Cancelar.Size = New System.Drawing.Size(90, 32)
        Me.B_Cancelar.TabIndex = 11
        Me.B_Cancelar.Text = "Cancelar"
        Me.B_Cancelar.UseVisualStyleBackColor = True
        '
        'L_Fecha
        '
        Me.L_Fecha.AutoSize = True
        Me.L_Fecha.Location = New System.Drawing.Point(12, 15)
        Me.L_Fecha.Name = "L_Fecha"
        Me.L_Fecha.Size = New System.Drawing.Size(51, 17)
        Me.L_Fecha.TabIndex = 12
        Me.L_Fecha.Text = "Fecha:"
        '
        'DTP_Fecha
        '
        Me.DTP_Fecha.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DTP_Fecha.CustomFormat = "dd/mm/yyyy"
        Me.DTP_Fecha.Location = New System.Drawing.Point(94, 12)
        Me.DTP_Fecha.Name = "DTP_Fecha"
        Me.DTP_Fecha.Size = New System.Drawing.Size(269, 22)
        Me.DTP_Fecha.TabIndex = 13
        '
        'NUD_Residentes
        '
        Me.NUD_Residentes.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.NUD_Residentes.Location = New System.Drawing.Point(176, 70)
        Me.NUD_Residentes.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.NUD_Residentes.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NUD_Residentes.Name = "NUD_Residentes"
        Me.NUD_Residentes.Size = New System.Drawing.Size(187, 22)
        Me.NUD_Residentes.TabIndex = 14
        Me.NUD_Residentes.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'L_Residentes
        '
        Me.L_Residentes.AutoSize = True
        Me.L_Residentes.Location = New System.Drawing.Point(12, 72)
        Me.L_Residentes.Name = "L_Residentes"
        Me.L_Residentes.Size = New System.Drawing.Size(158, 17)
        Me.L_Residentes.TabIndex = 15
        Me.L_Residentes.Text = "Residentes de Guardia:"
        '
        'FormExcepcion
        '
        Me.AcceptButton = Me.B_Aceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(120.0!, 120.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.CancelButton = Me.B_Cancelar
        Me.ClientSize = New System.Drawing.Size(375, 161)
        Me.Controls.Add(Me.L_Residentes)
        Me.Controls.Add(Me.NUD_Residentes)
        Me.Controls.Add(Me.DTP_Fecha)
        Me.Controls.Add(Me.L_Fecha)
        Me.Controls.Add(Me.P_Blanco)
        Me.Controls.Add(Me.P_Gris)
        Me.Controls.Add(Me.B_Aceptar)
        Me.Controls.Add(Me.B_Cancelar)
        Me.Controls.Add(Me.CB_Excepcion)
        Me.Controls.Add(Me.L_Pedido)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormExcepcion"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Excepcion"
        CType(Me.NUD_Residentes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents L_Pedido As Label
    Private WithEvents P_Blanco As Panel
    Private WithEvents P_Gris As Panel
    Private WithEvents B_Aceptar As Button
    Private WithEvents B_Cancelar As Button
    Private WithEvents CB_Excepcion As ComboBox
    Private WithEvents L_Fecha As Label
    Private WithEvents DTP_Fecha As DateTimePicker
    Private WithEvents NUD_Residentes As NumericUpDown
    Private WithEvents L_Residentes As Label
End Class
