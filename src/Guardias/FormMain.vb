﻿'**********************************************************************
'* Name: FormMain.vb
'* Desc: Formulario principal de la aplicación
'* Auth: Franco Rosatti
'* Date: Aug-2019
'* Vers: 1.0
'**********************************************************************

Imports HPdf

Public Class FormMain
#Region "INICIO Y FIN"
    Private Sub FormMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ResidenteData.Load()
        GuardiaData.Load()

        Me.CargarGuardias()
    End Sub
#End Region
#Region "VARIABLES Y PROPIEDADES"

#End Region
#Region "ACCIONES DE LOS USUARIOS"
#Region "Main Menu"

    Private Sub TSMI_Residentes_Click(sender As Object, e As EventArgs) Handles TSMI_Residentes.Click
        Me.AdministrarResidentes()
    End Sub
    Private Sub TSMI_Ayu_AcercaDe_Click(sender As Object, e As EventArgs) Handles TSMI_Ayu_AcercaDe.Click
        Me.AcercaDe()
    End Sub
#End Region
#Region "List View"
    Private Sub LV_Guardias_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LV_Guardias.SelectedIndexChanged
        If Me.LV_Guardias.SelectedItems.Count = 1 Then
            Me.TSB_ModificarGuardia.Enabled = True
            Me.TSB_EliminarGuardia.Enabled = True
        Else
            Me.TSB_ModificarGuardia.Enabled = False
            Me.TSB_EliminarGuardia.Enabled = False
        End If
    End Sub
    Private Sub LV_Guardias_DoubleClick(sender As Object, e As EventArgs) Handles LV_Guardias.DoubleClick
        Me.ModificarGuardia()
    End Sub
    Private Sub LV_Guardias_ColumnClick(sender As Object, e As ColumnClickEventArgs) Handles LV_Guardias.ColumnClick
        OrdenListas.OrdenarLista(e, Me.LV_Guardias)
    End Sub
#End Region
#Region "Context Menu"
    'TODO: Poner en el menu la opcion de regenerar y aclarar que la generacion del pdf no regenera la guardia, tal vez cambiar por ver guardia y volver a generar, y la unica forma de visualizar es generando el pdf
    Private Sub CMS_Guardias_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles CMS_Guardias.Opening
        If Me.LV_Guardias.SelectedItems.Count <> 1 Then
            e.Cancel = True
        End If
    End Sub
    Private Sub TMSI_GenerarPDF_Click(sender As Object, e As EventArgs) Handles TSMI_VerGuardia.Click
        If Me.LV_Guardias.SelectedItems.Count <> 1 Then Return
        Dim item As GuardiaItem = Me.LV_Guardias.SelectedItems(0)
        Me.VerGuardia(item.Guardia)
    End Sub
    Private Sub TSMI_VolverAGenerarGuardia_Click(sender As Object, e As EventArgs) Handles TSMI_VolverAGenerarGuardia.Click
        If Me.LV_Guardias.SelectedItems.Count <> 1 Then Return
        Dim item As GuardiaItem = Me.LV_Guardias.SelectedItems(0)
        Me.RegenerarGuardia(item.Guardia)
    End Sub
#End Region
#Region "ToolBar"
    Private Sub TSB_CrearGuardia_Click(sender As Object, e As EventArgs) Handles TSB_CrearGuardia.Click
        Me.CrearGuardia()
    End Sub
    Private Sub TSB_ModificarGuardia_Click(sender As Object, e As EventArgs) Handles TSB_ModificarGuardia.Click
        Me.ModificarGuardia()
    End Sub
    Private Sub TSB_EliminarGuardia_Click(sender As Object, e As EventArgs) Handles TSB_EliminarGuardia.Click
        Me.EliminarGuardia()
    End Sub
#End Region
#End Region
#Region "METODOS PRIVADOS"
    Private Sub AdministrarResidentes()
        Dim f As New FormGrupos()
        f.ShowDialog()
    End Sub
    Private Sub AcercaDe()
        Dim f As New FormAbout
        f.ShowDialog()
    End Sub
    Private Sub CargarGuardias()
        Me.LV_Guardias.BeginUpdate()
        Me.LV_Guardias.Items.Clear()
        For Each g As Guardia In GuardiaData.Guardias
            Me.LV_Guardias.Items.Add(New GuardiaItem(g))
        Next
        Me.LV_Guardias.EndUpdate()
    End Sub
    Private Sub CrearGuardia()
        Dim f As New FormGuardiaCreate(GuardiaData.Guardias)
        If f.ShowDialog = DialogResult.OK Then
            GuardiaData.Guardias.Add(f.Guardia)
            If Not GuardiaData.Save() Then
                GuardiaData.Guardias.Remove(f.Guardia)
                MsgBox("No fue posible crear la guardia.", Buttons:=MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly, Title:=TextoService.AppFullName)
                Return
            End If
            Me.LV_Guardias.Items.Add(New GuardiaItem(f.Guardia))
            Dim fcfg As New FormGuardiaConfig(f.Guardia, True)
            fcfg.ShowDialog()
            If Not GuardiaData.Save() Then
                MsgBox("No fue posible guardar la guardia.", Buttons:=MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly, Title:=TextoService.AppFullName)
                Return
            End If
        End If
    End Sub
    Private Sub ModificarGuardia()
        If Me.LV_Guardias.SelectedItems.Count <> 1 Then
            MsgBox("Seleccione un guardia.", Buttons:=MsgBoxStyle.Exclamation Or MsgBoxStyle.OkOnly, Title:=TextoService.AppFullName)
            Return
        End If

        Dim item As GuardiaItem = Me.LV_Guardias.SelectedItems(0)

        Dim f As New FormGuardiaConfig(item.Guardia, False)

        If f.ShowDialog() = DialogResult.OK Then
            If Not GuardiaData.Save() Then
                MsgBox("No fue posible guardar la guardia.", Buttons:=MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly, Title:=TextoService.AppFullName)
                Return
            End If
        End If
    End Sub
    Private Sub EliminarGuardia()
        If Me.LV_Guardias.SelectedItems.Count <> 1 Then
            MsgBox("Seleccione un guardia.", Buttons:=MsgBoxStyle.Exclamation Or MsgBoxStyle.OkOnly, Title:=TextoService.AppFullName)
            Return
        End If

        If MsgBox("¿Está seguro que desea eliminar la guardia?", Buttons:=MsgBoxStyle.Question Or MsgBoxStyle.YesNoCancel, Title:=TextoService.AppFullName) = MsgBoxResult.Yes Then
            Dim item As GuardiaItem = Me.LV_Guardias.SelectedItems(0)
            GuardiaData.Guardias.Remove(item.Guardia)
            If Not GuardiaData.Save() Then
                GuardiaData.Guardias.Add(item.Guardia)
                MsgBox("No fue posible eliminar la guardia.", Buttons:=MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly, Title:=TextoService.AppFullName)
                Return
            End If
            Me.LV_Guardias.Items.Remove(item)
        End If
    End Sub
#Region "PDF"
    Private Sub VerGuardia(g As Guardia)
        GuardiaService.GenerarGuardia(g, False)
        GenerarPdf(g)
    End Sub
    Private Sub RegenerarGuardia(g As Guardia)
        GuardiaService.GenerarGuardia(g, True)
        GenerarPdf(g)
    End Sub
    Private Sub VerResumen(g As Guardia)
        Dim cant(g.Grupo.Residentes.Count - 1) As Integer
        For Each ddg As DiaDeGuardia In g.Dias
            For Each r As Residente In ddg.Residentes
                cant(g.Grupo.Residentes.IndexOf(r)) += 1
            Next
        Next

        Dim str As String = ""

        For i As Integer = 0 To cant.Length - 1
            str += String.Format("{0}: {1}" + vbCrLf, g.Grupo.Residentes(i).Apellido, cant(i))
        Next

        MsgBox(Str, Buttons:=MsgBoxStyle.Information, Title:=TextoService.AppFullName)
    End Sub
    Private Sub GenerarPdf(g As Guardia)
        Dim sfd As New SaveFileDialog With {.AddExtension = True, .FileName = String.Format("{0}.pdf", g.Nombre), .Filter = "Pdf (*.pdf)|*.pdf|Todos los Archivos (*.*)|*.*", .Title = "Ingrese el nombre del archivo"}
        If sfd.ShowDialog <> DialogResult.OK Then Return

        Dim filename As String = sfd.FileName

        IO.File.Delete(filename)

        'extract the month
        Dim daysInMonth As Integer = DateTime.DaysInMonth(g.Año, g.Mes)
        Dim firstOfMonth As DateTime = New DateTime(g.Año, g.Mes, 1)
        Dim firstDayOfMonth As Integer = CInt(firstOfMonth.DayOfWeek)  'days of week starts by default as Sunday = 0
        Dim _Filas As Integer = Math.Ceiling((firstDayOfMonth + daysInMonth) / 7.0)
        Dim _Columnas As Integer = 7
        Dim Dias As String() = {"Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"}

        Try
            Dim pdf As HPdfDoc = New HPdfDoc()

            'Seteo el font
            Dim font As HPdfFont = pdf.GetFont("Helvetica", "WinAnsiEncoding")

            'Creo la pagina
            Dim page As HPdfPage = pdf.AddPage()
            page.SetSize(HPdfPageSizes.HPDF_PAGE_SIZE_A4, HPdfPageDirection.HPDF_PAGE_LANDSCAPE) 'Hoja apaisada

            'Dimensiones
            Dim _margen As Integer = 50
            Dim _hdh As Integer = 24 'Header Dias Height: "Lunes | Martes | Mierc...
            Dim _H As Single = page.GetHeight()
            Dim _W As Single = page.GetWidth()
            Dim col_width As Single = (_W - 2 * _margen) / _Columnas
            Dim row_height As Single = (_H - 2 * _margen - _hdh) / _Filas

            'Titulo
            page.SetFontAndSize(font, 24)
            Dim tw As Single = page.TextWidth(g.Nombre) 'tw: text width
            Dim th As Single = 24 'th: text height
            page.BeginText()
            page.MoveTextPos((_W - tw) / 2, _H - (_margen + th) / 2)
            page.ShowText(g.Nombre)
            page.EndText()

            'Marco
            page.SetLineWidth(1)
            'page.SetRGBStroke(0.6, 0.6, 0.6)
            page.Rectangle(_margen, _margen, _W - 2 * _margen, _H - 2 * _margen)
            page.Stroke()

            'Header Dias
            page.SetLineWidth(1)
            page.MoveTo(_margen, _H - _margen - _hdh)
            page.LineTo(_W - _margen, _H - _margen - _hdh)
            page.Stroke()
            page.SetFontAndSize(font, 12)
            th = 10
            For i As Integer = 0 To Dias.Length - 1
                tw = page.TextWidth(Dias(i))
                page.BeginText()
                page.MoveTextPos(_margen + i * col_width + (col_width - tw) / 2, _H - _margen - _hdh + (_hdh - th) / 2)
                page.ShowText(Dias(i))
                page.EndText()
            Next

            'Divisiones
            page.SetLineWidth(1)
            For i As Integer = 1 To _Columnas - 1
                page.MoveTo(_margen + i * col_width, _H - _margen)
                page.LineTo(_margen + i * col_width, _margen)
                page.Stroke()
            Next

            For i As Integer = 1 To _Filas - 1
                page.MoveTo(_margen, _margen + i * row_height)
                page.LineTo(_W - _margen, _margen + i * row_height)
                page.Stroke()
            Next

            'Numero de día
            page.SetFontAndSize(font, 10)
            th = 10
            Dim col As Integer = firstDayOfMonth
            Dim row As Integer = 0
            Dim subrow As Integer = 0
            For i As Integer = 0 To daysInMonth - 1
                'Imprimo el numero de dia
                tw = page.TextWidth((i + 1).ToString)
                page.BeginText()
                page.MoveTextPos(_margen + (col + 1) * col_width - tw - 2, _H - _margen - _hdh - th - row * row_height)
                page.ShowText((i + 1).ToString)
                page.EndText()

                'Paso a la siguiente columna
                col += 1

                'Si llego al final, pongo la columna en 0 y avanzo a la siguiente fila
                If col Mod 7 = 0 Then
                    col = 0
                    row += 1
                End If
            Next

            'Imprimo residentes
            page.SetFontAndSize(font, 10)
            th = 10
            subrow = 0
            For Each dia As DiaDeGuardia In g.Dias
                col = CInt(dia.Fecha.DayOfWeek)
                row = Math.Floor((dia.Fecha.Day + firstDayOfMonth - 1) / 7.0)

                subrow = 1
                For Each r As Residente In dia.Residentes
                    page.BeginText()
                    page.MoveTextPos(_margen + (col) * col_width + 6, _H - _margen - _hdh - th * (subrow + 1) - row * row_height - 4)
                    page.ShowText(r.Apellido)
                    page.EndText()
                    subrow += 1
                Next
            Next

            'Footer
            page.SetFontAndSize(font, 8)
            Dim footer As String = String.Format("Generado por {0} ({1})", TextoService.AppFullName, Now.ToString("HH:mm:ss dd/MM/yyyy"))
            tw = page.TextWidth(footer)
            th = 10
            page.BeginText()
            page.MoveTextPos(_W - tw - _margen, _margen / 2)
            page.ShowText(footer)
            page.EndText()

            pdf.SaveToFile(filename)

            System.Diagnostics.Process.Start(filename)
#If DEBUG Then
            Me.VerResumen(g)
#End If
        Catch ex As Exception
#If DEBUG Then
            MsgBox(ex.Message)
#End If
        End Try
    End Sub
    Private Sub DrawLine(ByRef page As HPdfPage, ByVal x As Single, ByVal y As Single, ByVal label As String)
        page.BeginText()
        page.MoveTextPos(x, y - 10)
        page.ShowText(label)
        page.EndText()

        page.MoveTo(x, y - 15)
        page.LineTo(x + 220, y - 15)
        page.Stroke()
    End Sub
    Private Sub DrawRect(ByRef page As HPdfPage, ByVal x As Single, ByVal y As Single, ByVal label As String)
        page.BeginText()
        page.MoveTextPos(x, y - 10)
        page.ShowText(label)
        page.EndText()

        page.Rectangle(x, y - 40, 220, 25)
    End Sub
#End Region
#End Region
#Region "CLASES PRIVADAS"
    Private Class GuardiaItem
        Inherits ListViewItem

        Public Property Guardia As Guardia

        Public Sub New(value As Guardia)
            Me.Guardia = value
            Me.Text = Me.Guardia.Grupo.Nombre
            Me.SubItems.Add(Me.Guardia.Año)
            Me.SubItems.Add(TextoService.MesToString(Me.Guardia.Mes))
        End Sub
    End Class
#End Region




    'DEBUG:
    Private Sub SetTestResidentes(g As Guardia)
        Dim rnd As New Random(Now.Millisecond)

        g.Dias.Clear()
        Dim ddg As DiaDeGuardia
        For i As Integer = 0 To DateTime.DaysInMonth(g.Año, g.Mes) - 1
            ddg = New DiaDeGuardia()
            ddg.Fecha = New Date(g.Año, g.Mes, i + 1)

            'Random residentes
            Dim idx As Integer = rnd.Next(0, g.Grupo.Residentes.Count - 3)
            For j As Integer = idx To idx + 2
                ddg.Residentes.Add(g.Grupo.Residentes(j))
            Next

            g.Dias.Add(ddg)
        Next
    End Sub
End Class