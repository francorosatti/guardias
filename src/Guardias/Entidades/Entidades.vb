﻿'**********************************************************************
'* Name: Entidades.vb
'* Desc: Entidades de la apliación
'* Auth: Franco Rosatti
'* Date: Aug-2019
'* Vers: 1.0
'**********************************************************************

Public Class Guardia
    Public Property Generada As Boolean = False
    Public Property Mes As Integer
    Public Property Año As Integer
    Public Property Grupo As Grupo
    Public ReadOnly Property Nombre As String
        Get
            Return String.Format("{0} - {1} {2}", Me.Grupo.Nombre, TextoService.MesToString(Me.Mes), Me.Año)
        End Get
    End Property

    Dim m_ResidentesDeGuardia() As Integer = {0, 0, 0, 0, 0, 0, 0}

    Public ReadOnly Property ResidentesDeGuardia As Integer()
        Get
            Return Me.m_ResidentesDeGuardia
        End Get
    End Property
    Public ReadOnly Property ResidentesDeGuardia(index As Integer) As Integer
        Get
            Return Me.m_ResidentesDeGuardia(index)
        End Get
    End Property
    Public ReadOnly Property HayGuardia(index As Integer) As Boolean
        Get
            Return (Me.m_ResidentesDeGuardia(index) > 0)
        End Get
    End Property

    Public ReadOnly Property Lunes As Boolean
        Get
            Return (Me.m_ResidentesDeGuardia(1) > 0)
        End Get
    End Property
    Public Property ResidentesLunes As Integer
        Get
            Return Me.m_ResidentesDeGuardia(1)
        End Get
        Set(value As Integer)
            Me.m_ResidentesDeGuardia(1) = value
        End Set
    End Property
    Public ReadOnly Property Martes As Boolean
        Get
            Return (Me.m_ResidentesDeGuardia(2) > 0)
        End Get
    End Property
    Public Property ResidentesMartes As Integer
        Get
            Return Me.m_ResidentesDeGuardia(2)
        End Get
        Set(value As Integer)
            Me.m_ResidentesDeGuardia(2) = value
        End Set
    End Property
    Public ReadOnly Property Miercoles As Boolean
        Get
            Return (Me.m_ResidentesDeGuardia(3) > 0)
        End Get
    End Property
    Public Property ResidentesMiercoles As Integer
        Get
            Return Me.m_ResidentesDeGuardia(3)
        End Get
        Set(value As Integer)
            Me.m_ResidentesDeGuardia(3) = value
        End Set
    End Property
    Public ReadOnly Property Jueves As Boolean
        Get
            Return (Me.m_ResidentesDeGuardia(4) > 0)
        End Get
    End Property
    Public Property ResidentesJueves As Integer
        Get
            Return Me.m_ResidentesDeGuardia(4)
        End Get
        Set(value As Integer)
            Me.m_ResidentesDeGuardia(4) = value
        End Set
    End Property
    Public ReadOnly Property Viernes As Boolean
        Get
            Return (Me.m_ResidentesDeGuardia(5) > 0)
        End Get
    End Property
    Public Property ResidentesViernes As Integer
        Get
            Return Me.m_ResidentesDeGuardia(5)
        End Get
        Set(value As Integer)
            Me.m_ResidentesDeGuardia(5) = value
        End Set
    End Property
    Public ReadOnly Property Sabado As Boolean
        Get
            Return (Me.m_ResidentesDeGuardia(6) > 0)
        End Get
    End Property
    Public Property ResidentesSabado As Integer
        Get
            Return Me.m_ResidentesDeGuardia(6)
        End Get
        Set(value As Integer)
            Me.m_ResidentesDeGuardia(6) = value
        End Set
    End Property
    Public ReadOnly Property Domingo As Boolean
        Get
            Return (Me.m_ResidentesDeGuardia(0) > 0)
        End Get
    End Property
    Public Property ResidentesDomingo As Integer
        Get
            Return Me.m_ResidentesDeGuardia(0)
        End Get
        Set(value As Integer)
            Me.m_ResidentesDeGuardia(0) = value
        End Set
    End Property

    Public ReadOnly Property Total As Integer
        Get
            Return Me.m_ResidentesDeGuardia.Sum()
        End Get
    End Property

    Public Property Pedidos As New List(Of Pedido)

    Public Property Excepciones As New List(Of Excepcion)

    Public Property Dias As New List(Of DiaDeGuardia)
End Class

Public Class DiaDeGuardia
    Public Fecha As Date
    Public Residentes As New List(Of Residente)
End Class

Public Class Grupo
    Public Property Nombre As String
    Public Residentes As New List(Of Residente)

    Public Function Clone() As Grupo
        Dim c As Grupo = Me.MemberwiseClone()
        c.Residentes = New List(Of Residente)
        For Each r As Residente In Me.Residentes
            c.Residentes.Add(r.Clone)
        Next
        Return c
    End Function

    Public Sub UnClone(value As Grupo)
        Me.Nombre = value.Nombre
        Me.Residentes = value.Residentes
    End Sub
End Class

Public Class Residente
    Public Property Nombre As String
    Public Property Apellido As String

    Public ReadOnly Property NombreCompleto As String
        Get
            Return Me.Nombre + " " + Me.Apellido
        End Get
    End Property
    Public Function Clone() As Residente
        Return Me.MemberwiseClone()
    End Function
End Class

Public Class Pedido
    Public Property Tipo As ETipo = ETipo.Pedido
    Public Property Residente As Residente
    Public Property Desde As Date
    Public Property Hasta As Date
    Public Property Trabajar As Boolean

    Public ReadOnly Property TrabajarString As String
        Get
            If Me.Tipo = ETipo.Pedido Then
                If Me.Trabajar Then Return "Trabajar" Else Return "No Trabajar"
            ElseIf Me.Tipo = ETipo.Vacaciones Then
                Return "Vacaciones"
            Else
                Return "- - -"
            End If
        End Get
    End Property

    Public Enum ETipo
        None = -1
        Pedido = 0
        Vacaciones = 1
    End Enum
End Class

Public Class Excepcion
    Public Property Fecha As Date
    Public Property Laboral As Boolean = True
    Public Property ResidentesDeGuardia As Integer

    Public ReadOnly Property LaboralString As String
        Get
            If Laboral Then Return "Laboral" Else Return "No Laboral"
        End Get
    End Property
End Class