﻿'**********************************************************************
'* Name: OrdenListas.vb
'* Desc: Servicio para ordenar las listas por columna
'* Auth: Franco Rosatti
'* Date: Aug-2019
'* Vers: 1.0
'**********************************************************************

Public Class OrdenListas
    Private Shared m_sortColumn As Integer = -1
    Public Shared Sub OrdenarLista(ByVal e As ColumnClickEventArgs, ByVal lv As ListView)
        If e.Column <> m_sortColumn Then
            m_sortColumn = e.Column
            lv.Sorting = SortOrder.Ascending
        Else
            If lv.Sorting = SortOrder.Ascending Then
                lv.Sorting = SortOrder.Descending
            Else
                lv.Sorting = SortOrder.Ascending
            End If
        End If
        lv.Sort()
        lv.ListViewItemSorter = New ListViewItemComparer(e.Column, lv.Sorting)
    End Sub

    Private Class ListViewItemComparer
        Implements IComparer
        Dim m_Col As Integer
        Dim m_Ord As SortOrder
        Public Sub New()
            Me.m_Col = 0
            Me.m_Ord = SortOrder.Ascending
        End Sub
        Public Sub New(column As Integer, order As SortOrder)
            Me.m_Col = column
            Me.m_Ord = order
        End Sub
        Public Function Compare(x As Object, y As Object) As Integer Implements System.Collections.IComparer.Compare
            Dim returnVal As Integer = -1
            returnVal = [String].Compare(CType(x, ListViewItem).SubItems(Me.m_Col).Text, CType(y, ListViewItem).SubItems(Me.m_Col).Text)
            If Me.m_Ord = SortOrder.Descending Then
                returnVal *= -1
            End If
            Return returnVal
        End Function
    End Class
End Class
