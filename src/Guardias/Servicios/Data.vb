﻿'**********************************************************************
'* Name: GrupoService.vb
'* Desc: Servicios para los grupos de residentes.
'* Auth: Franco Rosatti
'* Date: Aug-2019
'* Vers: 1.0
'**********************************************************************

Imports System.IO
Imports Newtonsoft.Json

Public Class ResidenteData
    Public Shared Property Grupos As List(Of Grupo)
    Public Shared Sub Load()
        Grupos = ReadGrupos()
        If Grupos Is Nothing Then Grupos = New List(Of Grupo)
    End Sub
    Public Shared Function Save() As Boolean
        Return WriteGrupos(Grupos)
    End Function
#Region "Private"
    Private Shared GRUPOS_DIR As String = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\" + TextoService.AppName
    Private Shared GRUPOS_PATH As String = GRUPOS_DIR + "\Grupos.json"
    Private Shared Function ReadGrupos() As List(Of Grupo)
        If Not File.Exists(GRUPOS_PATH) Then Return Nothing

        Dim fs As FileStream
        Dim rd As StreamReader
        Dim ret As List(Of Grupo) = Nothing

        Try
            fs = New FileStream(GRUPOS_PATH, FileMode.Open, FileAccess.Read)
        Catch ex As Exception
            Return Nothing
        End Try

        Try
            rd = New StreamReader(fs)
        Catch ex As Exception
            Try
                fs.Close()
            Catch ex1 As Exception : End Try
            Return Nothing
        End Try

        Try
            Dim json As String = rd.ReadToEnd()
            ret = JsonConvert.DeserializeObject(Of List(Of Grupo))(json)
        Catch ex As Exception
        End Try

        Try
            rd.Close()
        Catch ex As Exception : End Try

        Try
            fs.Close()
        Catch ex As Exception : End Try

        Return ret
    End Function
    Private Shared Function WriteGrupos(value As List(Of Grupo)) As Boolean
        If Not Directory.Exists(GRUPOS_DIR) Then
            Try
                Directory.CreateDirectory(GRUPOS_DIR)
            Catch ex As Exception
                Return False
            End Try
        End If

        Dim fs As FileStream
        Dim wr As StreamWriter
        Dim ret As Boolean = False

        Try
            fs = New FileStream(GRUPOS_PATH, FileMode.Create, FileAccess.ReadWrite)
        Catch ex As Exception
            Return False
        End Try

        Try
            wr = New StreamWriter(fs)
        Catch ex As Exception
            Try
                fs.Close()
            Catch ex1 As Exception : End Try
            Return False
        End Try

        Try
            Dim json As String = JsonConvert.SerializeObject(value)
            wr.Write(json)
            ret = True
        Catch ex As Exception
        End Try

        Try
            wr.Close()
        Catch ex As Exception : End Try

        Try
            fs.Close()
        Catch ex As Exception : End Try

        Return ret
    End Function
#End Region
End Class

Public Class GuardiaData
    Public Shared Property Guardias As List(Of Guardia)
    Public Shared Sub Load()
        Guardias = ReadGuardias()
        If Guardias Is Nothing Then Guardias = New List(Of Guardia)
    End Sub
    Public Shared Function Save() As Boolean
        Return WriteGuardias(Guardias)
    End Function
#Region "Private"
    Private Shared GUARDIAS_DIR As String = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\" + TextoService.AppName
    Private Shared GUARDIAS_PATH As String = GUARDIAS_DIR + "\Guardias.json"
    Private Shared Function ReadGuardias() As List(Of Guardia)
        If Not File.Exists(GUARDIAS_PATH) Then Return Nothing

        Dim fs As FileStream
        Dim rd As StreamReader
        Dim ret As List(Of Guardia) = Nothing

        Try
            fs = New FileStream(GUARDIAS_PATH, FileMode.Open, FileAccess.Read)
        Catch ex As Exception
            Return Nothing
        End Try

        Try
            rd = New StreamReader(fs)
        Catch ex As Exception
            Try
                fs.Close()
            Catch ex1 As Exception : End Try
            Return Nothing
        End Try

        Try
            Dim json As String = rd.ReadToEnd()
            ret = JsonConvert.DeserializeObject(Of List(Of Guardia))(json)
            For Each g As Guardia In ret
                For Each gr As Grupo In ResidenteData.Grupos
                    If g.Grupo.Nombre = gr.Nombre Then
                        g.Grupo = gr
                    End If
                Next
                For Each p As Pedido In g.Pedidos
                    For Each r As Residente In g.Grupo.Residentes
                        If p.Residente.NombreCompleto = r.NombreCompleto Then
                            p.Residente = r
                        End If
                    Next
                Next
                For Each ddg As DiaDeGuardia In g.Dias
                    Dim lst As New List(Of Residente)
                    For Each r1 As Residente In ddg.Residentes
                        For Each r2 As Residente In g.Grupo.Residentes
                            If r1.NombreCompleto = r2.NombreCompleto Then
                                lst.Add(r2)
                            End If
                        Next
                    Next
                    ddg.Residentes = lst
                Next
            Next
        Catch ex As Exception
        End Try

        Try
            rd.Close()
        Catch ex As Exception : End Try

        Try
            fs.Close()
        Catch ex As Exception : End Try

        Return ret
    End Function
    Private Shared Function WriteGuardias(value As List(Of Guardia)) As Boolean
        If Not Directory.Exists(GUARDIAS_DIR) Then
            Try
                Directory.CreateDirectory(GUARDIAS_DIR)
            Catch ex As Exception
                Return False
            End Try
        End If

        Dim fs As FileStream
        Dim wr As StreamWriter
        Dim ret As Boolean = False

        Try
            fs = New FileStream(GUARDIAS_PATH, FileMode.Create, FileAccess.ReadWrite)
        Catch ex As Exception
            Return False
        End Try

        Try
            wr = New StreamWriter(fs)
        Catch ex As Exception
            Try
                fs.Close()
            Catch ex1 As Exception : End Try
            Return False
        End Try

        Try
            Dim json As String = JsonConvert.SerializeObject(value)
            wr.Write(json)
            ret = True
        Catch ex As Exception
        End Try

        Try
            wr.Close()
        Catch ex As Exception : End Try

        Try
            fs.Close()
        Catch ex As Exception : End Try

        Return ret
    End Function
#End Region
End Class