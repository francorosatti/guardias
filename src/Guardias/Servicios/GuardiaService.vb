﻿'**********************************************************************
'* Name: GuardiaService.vb
'* Desc: Servicio para generar una guardia a partir de sus reglas
'* Auth: Franco Rosatti
'* Date: Sep-2019
'* Vers: 1.0
'**********************************************************************

Public Class GuardiaService
    Private Shared m_Guardia As Guardia = Nothing
    Private Shared rnd As New Random(Now.Millisecond)

    Public Shared Sub GenerarGuardia(g As Guardia, regenerar As Boolean)
        If Not regenerar AndAlso g.Generada Then Return

        m_Guardia = g
        m_Guardia.Dias.Clear()

        Dim isExcep As Boolean = False
        Dim exc As Excepcion = Nothing
        Dim ddg As DiaDeGuardia = Nothing
        Dim residentesdeguardia As Integer = 0
        Dim d As Date

        Dim total_de_guardias As Integer = 0
        Dim max_guardias As Integer = 0
        'Obtengo el total de guardias = dias de guardia x cantidad de residentes de guardia ese dia
        For dia_index As Integer = 0 To Date.DaysInMonth(m_Guardia.Año, m_Guardia.Mes) - 1
            d = New Date(m_Guardia.Año, m_Guardia.Mes, dia_index + 1)

            'Busco excepciones o feriados
            exc = Nothing
            For Each e As Excepcion In m_Guardia.Excepciones
                If e.Fecha = d Then
                    exc = e
                    Exit For
                End If
            Next

            If exc IsNot Nothing AndAlso Not exc.Laboral Then Continue For
            If exc Is Nothing AndAlso Not m_Guardia.HayGuardia(d.DayOfWeek) Then Continue For

            If exc IsNot Nothing Then
                total_de_guardias += exc.ResidentesDeGuardia
            Else
                total_de_guardias += m_Guardia.ResidentesDeGuardia(d.DayOfWeek)
            End If
        Next
        max_guardias = Math.Ceiling(total_de_guardias / m_Guardia.Grupo.Residentes.Count)

        For dia_index As Integer = 0 To Date.DaysInMonth(m_Guardia.Año, m_Guardia.Mes) - 1
            d = New Date(m_Guardia.Año, m_Guardia.Mes, dia_index + 1)

            'Busco excepciones o feriados
            isExcep = False
            For Each e As Excepcion In m_Guardia.Excepciones
                If e.Fecha = d Then
                    isExcep = True
                    exc = e
                    Exit For
                End If
            Next

            If isExcep AndAlso Not exc.Laboral Then Continue For
            If Not isExcep AndAlso Not m_Guardia.HayGuardia(d.DayOfWeek) Then Continue For

            'Hacer guardia
            ddg = New DiaDeGuardia
            ddg.Fecha = d

            'Asignar residentes
            If isExcep Then
                residentesdeguardia = exc.ResidentesDeGuardia
            Else
                residentesdeguardia = m_Guardia.ResidentesDeGuardia(d.DayOfWeek)
            End If

            Dim r As Residente = Nothing
            For j As Integer = 1 To residentesdeguardia
                GuardiaService.AddResidente(ddg, max_guardias)
            Next

            m_Guardia.Dias.Add(ddg)
        Next

        m_Guardia.Generada = True
    End Sub
    Private Shared Function AddResidente(ddg As DiaDeGuardia, max As Integer) As Boolean
        'Si alguien pidió trabajar este dia y aun no fue asignado lo elijo

        Dim p1 As Pedido = m_Guardia.Pedidos.FirstOrDefault(Function(x) x.Tipo = Pedido.ETipo.Pedido AndAlso x.Trabajar AndAlso (x.Desde = ddg.Fecha OrElse x.Hasta = ddg.Fecha) AndAlso Not ddg.Residentes.Contains(x.Residente))
        If p1 IsNot Nothing Then
            ddg.Residentes.Add(p1.Residente)
            Return True
        End If

        'Armo la lista de candidatos
        Dim candidatos As New List(Of Residente)
        For Each r As Residente In m_Guardia.Grupo.Residentes
            'Si ya fue asignado a este dia de guardia no lo agrego
            If Not ddg.Residentes.Contains(r) Then candidatos.Add(r)
        Next
        'Quito a los que tienen vacaciones o se pidieron el dia
        For Each p2 As Pedido In m_Guardia.Pedidos.Where(Function(x) (x.Tipo = Pedido.ETipo.Vacaciones OrElse (x.Tipo = Pedido.ETipo.Pedido AndAlso Not x.Trabajar)) AndAlso x.Desde <= ddg.Fecha AndAlso x.Hasta >= ddg.Fecha)
            candidatos.Remove(p2.Residente)
        Next

        'Si no hay candidatos retorno
        If candidatos.Count <= 0 Then Return False

        'Asigno las probabilidades de acuerdo a que tan lejos esta su ultima guardia y de si este mes tienen vacaciones
        Dim Scores(candidatos.Count - 1) As Single
        For i As Integer = 0 To candidatos.Count - 1
            Scores(i) = GetScore(candidatos(i), ddg.Fecha, max)
        Next

        'Elijo al azar segun probabilidad
        Dim value As Double = Scores.Sum() * rnd.NextDouble()
        Dim acc As Single = 0
        For candidato_index As Integer = 0 To Scores.Length - 1
            acc += Scores(candidato_index)
            If value < acc Then
                ddg.Residentes.Add(candidatos(candidato_index))
                Return True
            End If
        Next
        Return False
    End Function
    Private Shared Function GetScore(r As Residente, d As DateTime, max As Integer) As Single
        Dim ds As Single = GetDistanceScore(r, d)
        Dim vs As Single = GetVacationScore(r)
        Dim rs As Single = GetRemainingScore(r, max)
        Return ds * vs * rs
    End Function
    ''' <summary>
    ''' Retorna el puntaje de acuerdo a la distancia en dias con la guardia asignada mas cercana y de la cantidad de guardias ya asignadas
    ''' </summary>
    Private Shared Function GetDistanceScore(r As Residente, d As DateTime) As Single
        Dim score As Single = Date.DaysInMonth(m_Guardia.Año, m_Guardia.Mes) 'lo mas lejos que puede estar
        For Each ddg As DiaDeGuardia In m_Guardia.Dias
            If ddg.Residentes.Contains(r) Then
                score = Math.Min(score, Math.Abs((ddg.Fecha - d).Days))
            End If
        Next
        Return Math.Min(10, score - 1)
    End Function
    ''' <summary>
    ''' Retorna el puntaje de acuerdo a si este mes tiene dias libres o vacaciones
    ''' </summary>
    Private Shared Function GetVacationScore(r As Residente) As Single
        Dim score As Single = 1
        For Each p As Pedido In m_Guardia.Pedidos.Where(Function(x) x.Tipo = Pedido.ETipo.Vacaciones)
            If p.Residente Is r Then
                score *= 2 * Math.Round(4 * (p.Hasta - p.Desde).Days / Date.DaysInMonth(m_Guardia.Año, m_Guardia.Mes))
            End If
        Next
        Return score
    End Function
    ''' <summary>
    ''' Retorna el puntaje de acuerdo a la cantidad de guardias ya asignadas
    ''' </summary>
    Private Shared Function GetRemainingScore(r As Residente, max As Integer) As Single
        Dim score As Single = 0
        For Each ddg As DiaDeGuardia In m_Guardia.Dias
            If ddg.Residentes.Contains(r) Then
                score += 1
            End If
        Next
        If score = max Then Return 0 'Si ya hizo el maximo de guardias de ese mes retorno cero
        Return 1 / (1 + 20 * score)
    End Function
End Class
