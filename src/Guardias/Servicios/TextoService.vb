﻿'**********************************************************************
'* Name: TextoService.vb
'* Desc: Servicios de conversión a texto
'* Auth: Franco Rosatti
'* Date: Aug-2018
'* Vers: 1.0
'**********************************************************************

Public Class TextoService
    Public Shared ReadOnly Property AppFullName As String
        Get
            Return AppName + " " + AppVersion
        End Get
    End Property
    Public Const AppName As String = "Guardias"
    Public Const AppVersion As String = "1.0"

    Public Shared Function MesToString(value As Integer) As String
        Select Case value
            Case 1 : Return "Enero"
            Case 2 : Return "Febrero"
            Case 3 : Return "Marzo"
            Case 4 : Return "Abril"
            Case 5 : Return "Mayo"
            Case 6 : Return "Junio"
            Case 7 : Return "Julio"
            Case 8 : Return "Agosto"
            Case 9 : Return "Septiembre"
            Case 10 : Return "Octubre"
            Case 11 : Return "Noviembre"
            Case 12 : Return "Diciembre"
            Case Else : Return "?"
        End Select
    End Function
End Class
