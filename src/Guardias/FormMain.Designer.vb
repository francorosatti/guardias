﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormMain))
        Me.TS_Guardias = New System.Windows.Forms.ToolStrip()
        Me.TSB_CrearGuardia = New System.Windows.Forms.ToolStripButton()
        Me.TSB_ModificarGuardia = New System.Windows.Forms.ToolStripButton()
        Me.TSB_EliminarGuardia = New System.Windows.Forms.ToolStripButton()
        Me.MS_Main = New System.Windows.Forms.MenuStrip()
        Me.TSMI_Residentes = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSMI_Ayuda = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSMI_Ayu_AcercaDe = New System.Windows.Forms.ToolStripMenuItem()
        Me.LV_Guardias = New System.Windows.Forms.ListView()
        Me.CH_Grupo = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.CH_Año = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.CH_Mes = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.CMS_Guardias = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.TSMI_VerGuardia = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSMI_VolverAGenerarGuardia = New System.Windows.Forms.ToolStripMenuItem()
        Me.TS_Guardias.SuspendLayout()
        Me.MS_Main.SuspendLayout()
        Me.CMS_Guardias.SuspendLayout()
        Me.SuspendLayout()
        '
        'TS_Guardias
        '
        Me.TS_Guardias.AutoSize = False
        Me.TS_Guardias.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.TS_Guardias.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.TS_Guardias.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSB_CrearGuardia, Me.TSB_ModificarGuardia, Me.TSB_EliminarGuardia})
        Me.TS_Guardias.Location = New System.Drawing.Point(0, 28)
        Me.TS_Guardias.Name = "TS_Guardias"
        Me.TS_Guardias.Size = New System.Drawing.Size(602, 40)
        Me.TS_Guardias.TabIndex = 0
        '
        'TSB_CrearGuardia
        '
        Me.TSB_CrearGuardia.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.TSB_CrearGuardia.Image = CType(resources.GetObject("TSB_CrearGuardia.Image"), System.Drawing.Image)
        Me.TSB_CrearGuardia.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_CrearGuardia.Name = "TSB_CrearGuardia"
        Me.TSB_CrearGuardia.Size = New System.Drawing.Size(48, 37)
        Me.TSB_CrearGuardia.Text = "Crear"
        '
        'TSB_ModificarGuardia
        '
        Me.TSB_ModificarGuardia.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.TSB_ModificarGuardia.Enabled = False
        Me.TSB_ModificarGuardia.Image = CType(resources.GetObject("TSB_ModificarGuardia.Image"), System.Drawing.Image)
        Me.TSB_ModificarGuardia.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_ModificarGuardia.Name = "TSB_ModificarGuardia"
        Me.TSB_ModificarGuardia.Size = New System.Drawing.Size(77, 37)
        Me.TSB_ModificarGuardia.Text = "Modificar"
        '
        'TSB_EliminarGuardia
        '
        Me.TSB_EliminarGuardia.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.TSB_EliminarGuardia.Enabled = False
        Me.TSB_EliminarGuardia.Image = CType(resources.GetObject("TSB_EliminarGuardia.Image"), System.Drawing.Image)
        Me.TSB_EliminarGuardia.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_EliminarGuardia.Name = "TSB_EliminarGuardia"
        Me.TSB_EliminarGuardia.Size = New System.Drawing.Size(67, 37)
        Me.TSB_EliminarGuardia.Text = "Eliminar"
        '
        'MS_Main
        '
        Me.MS_Main.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MS_Main.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSMI_Residentes, Me.TSMI_Ayuda})
        Me.MS_Main.Location = New System.Drawing.Point(0, 0)
        Me.MS_Main.Name = "MS_Main"
        Me.MS_Main.Size = New System.Drawing.Size(602, 28)
        Me.MS_Main.TabIndex = 1
        '
        'TSMI_Residentes
        '
        Me.TSMI_Residentes.Name = "TSMI_Residentes"
        Me.TSMI_Residentes.Size = New System.Drawing.Size(92, 24)
        Me.TSMI_Residentes.Text = "&Residentes"
        '
        'TSMI_Ayuda
        '
        Me.TSMI_Ayuda.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSMI_Ayu_AcercaDe})
        Me.TSMI_Ayuda.Name = "TSMI_Ayuda"
        Me.TSMI_Ayuda.Size = New System.Drawing.Size(63, 24)
        Me.TSMI_Ayuda.Text = "A&yuda"
        '
        'TSMI_Ayu_AcercaDe
        '
        Me.TSMI_Ayu_AcercaDe.Name = "TSMI_Ayu_AcercaDe"
        Me.TSMI_Ayu_AcercaDe.Size = New System.Drawing.Size(212, 26)
        Me.TSMI_Ayu_AcercaDe.Text = "&Acerca de Guardias"
        '
        'LV_Guardias
        '
        Me.LV_Guardias.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.CH_Grupo, Me.CH_Año, Me.CH_Mes})
        Me.LV_Guardias.ContextMenuStrip = Me.CMS_Guardias
        Me.LV_Guardias.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LV_Guardias.FullRowSelect = True
        Me.LV_Guardias.Location = New System.Drawing.Point(0, 68)
        Me.LV_Guardias.MultiSelect = False
        Me.LV_Guardias.Name = "LV_Guardias"
        Me.LV_Guardias.Size = New System.Drawing.Size(602, 285)
        Me.LV_Guardias.TabIndex = 2
        Me.LV_Guardias.UseCompatibleStateImageBehavior = False
        Me.LV_Guardias.View = System.Windows.Forms.View.Details
        '
        'CH_Grupo
        '
        Me.CH_Grupo.Text = "Grupo"
        Me.CH_Grupo.Width = 240
        '
        'CH_Año
        '
        Me.CH_Año.Text = "Año"
        Me.CH_Año.Width = 120
        '
        'CH_Mes
        '
        Me.CH_Mes.Text = "Mes"
        Me.CH_Mes.Width = 180
        '
        'CMS_Guardias
        '
        Me.CMS_Guardias.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.CMS_Guardias.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSMI_VerGuardia, Me.TSMI_VolverAGenerarGuardia})
        Me.CMS_Guardias.Name = "CMS_Guardias"
        Me.CMS_Guardias.Size = New System.Drawing.Size(211, 80)
        '
        'TSMI_VerGuardia
        '
        Me.TSMI_VerGuardia.Name = "TSMI_VerGuardia"
        Me.TSMI_VerGuardia.Size = New System.Drawing.Size(210, 24)
        Me.TSMI_VerGuardia.Text = "Ver &Guardia"
        '
        'TSMI_VolverAGenerarGuardia
        '
        Me.TSMI_VolverAGenerarGuardia.Name = "TSMI_VolverAGenerarGuardia"
        Me.TSMI_VolverAGenerarGuardia.Size = New System.Drawing.Size(210, 24)
        Me.TSMI_VolverAGenerarGuardia.Text = "&Volver a Generar"
        '
        'FormMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(120.0!, 120.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.ClientSize = New System.Drawing.Size(602, 353)
        Me.Controls.Add(Me.LV_Guardias)
        Me.Controls.Add(Me.TS_Guardias)
        Me.Controls.Add(Me.MS_Main)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MS_Main
        Me.MinimumSize = New System.Drawing.Size(600, 400)
        Me.Name = "FormMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Guardias 1.0"
        Me.TS_Guardias.ResumeLayout(False)
        Me.TS_Guardias.PerformLayout()
        Me.MS_Main.ResumeLayout(False)
        Me.MS_Main.PerformLayout()
        Me.CMS_Guardias.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents LV_Guardias As ListView
    Private WithEvents TS_Guardias As ToolStrip
    Private WithEvents TSMI_Residentes As ToolStripMenuItem
    Private WithEvents TSMI_Ayuda As ToolStripMenuItem
    Private WithEvents TSMI_Ayu_AcercaDe As ToolStripMenuItem
    Private WithEvents TSB_CrearGuardia As ToolStripButton
    Private WithEvents TSB_ModificarGuardia As ToolStripButton
    Private WithEvents TSB_EliminarGuardia As ToolStripButton
    Private WithEvents MS_Main As MenuStrip
    Private WithEvents CH_Grupo As ColumnHeader
    Private WithEvents CH_Año As ColumnHeader
    Private WithEvents CH_Mes As ColumnHeader
    Private WithEvents CMS_Guardias As ContextMenuStrip
    Private WithEvents TSMI_VerGuardia As ToolStripMenuItem
    Private WithEvents TSMI_VolverAGenerarGuardia As ToolStripMenuItem
End Class
